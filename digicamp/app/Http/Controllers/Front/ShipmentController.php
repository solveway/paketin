<?php namespace digipos\Http\Controllers\Front;
use Illuminate\Http\Request;

use digipos\models\Shipment;
use digipos\models\City;

class ShipmentController extends ShukakuController {

	public function __construct(){
		parent::__construct();

		$this->middleware($this->auth_guard)->only(['profile', 'edit_profile', 'change_password']);
		$this->menu = $this->data['path'][0];
		$this->model = new Shipment;
		$this->data['menu'] 		= $this->menu;
	}

	public function create(){
		$this->data['title'] 	= 'Form Kirim Barang';
		$city_exist 			= $this->model->where('city_id', '!=', null)->get();
		
		$arr = [];
		if($city_exist){
			foreach ($city_exist as $key => $value) {
				array_push($arr, $value->city_id);
			}
		}

		$this->data['city'] 	= City::whereNotIn('id', $arr)->get();
		
		return $this->render_view('pages.pages.shipment.create');
	}

	private function subscribe(){
		$request 			= $this->data['res'];
		$cek 				= Subscriber::where('email',$request->email)->first();
		if($cek){
			$response 			= ['status' => 'danger','text' => 'you already subscribe'];
		}else{
			$subscriber 		= new Subscriber;
			$subscriber->email 	= $request->email;
			$subscriber->save();
			$response 			= ['status' => 'success','text' => 'Thank you for subscribe'];
		}
		return response()->json($response);
	}
}
