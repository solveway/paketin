<?php namespace digipos\Http\Controllers\Admin;

use DB;
use Session;
use Hash;
use File;
use Excel;
use Schema;

use digipos\models\User;
use digipos\models\Station;
use digipos\models\Msmenu;
use digipos\models\Useraccess;
use digipos\models\Mslanguage;
use digipos\models\City;
use digipos\models\Shipment;
use digipos\models\Shipment_log;
use digipos\models\Shipment_size_log;
use digipos\models\Shipment_status_log;
use digipos\models\Shipment_status;
use digipos\models\City_delivery_price;
use digipos\models\Service_delivery;
use digipos\models\Config;
use digipos\models\Agent;

use digipos\Libraries\Report;
use digipos\Libraries\Pushnotification;
use digipos\Libraries\Email;

use digipos\Libraries\Alert;
use Illuminate\Http\Request;

class ShipmentController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 				= "Manage Shipment";
		$this->root_url				= "manage-shipment";
		$this->root_link 			= "manage-shipment";
		$this->model 				= new Shipment;
		$this->shipment_status_log 	= new Shipment_status_log;
		$this->shipment_size_log 	= new Shipment_size_log;
		$this->shipment_status 		= new Shipment_status;
		$this->image_path 			= 'components/both/images/shipment/';
		$this->image_path2 			= 'components/both/images/web/';
		$this->data['image_path'] 	= $this->image_path;
		$this->data['image_path2'] 	= $this->image_path2;
		$this->data['root_url']		= $this->root_url;

		$this->measurement = ['weight', 'volume'];
		// get city id allowed
		$this->city_id_allowed 			= Config::where('name', 'city_id')->first();
		$this->shipment_status 	= Config::where('name', 'city_id')->first();
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		// $this->field = [
		// 	[
		// 		'name' => 'shipping_no',
		// 		'label' => 'Shipping No',
		// 		'sorting' => 'y',
		// 		'search' => 'text',
		// 	],
		// 	[
		// 		'name' => 'agent_name',
		// 		'label' => 'Agent Name',
		// 		'sorting' => 'y',
		// 		'search' => 'select',
		// 	],
		// 	// [
		// 	// 	'name' => 'Start Station',
		// 	// 	'label' => 'Start Station',
		// 	// 	'search' => 'text',
		// 	// 	'search' => 'select',
		// 	// ],
		// 	// [
		// 	// 	'name' => 'Destination Station',
		// 	// 	'label' => 'Destination Station',
		// 	// 	'search' => 'text',
		// 	// 	'search' => 'select',
		// 	// ],
		// 	[
		// 		'name' => 'status_name',
		// 		'label' => 'Shipment Status',
		// 		'sorting' => 'y',
		// 		'search' => 'select'
		// 	],
		// ];

		$this->model = $this->model->leftJoin('agent', 'agent.id', 'shipment.agent_id')->join('shipment_status', 'shipment_status.id', 'shipment.shipment_status_id')->select('shipment.*', 'agent.business_name as agent_name', 'shipment_status.status_name as status_name');
		
		// return $this->build('index');
		$this->title 			= 'Shipment';
		$this->data['title'] 	= 'List '.$this->title;
		$this->data['data'] 	= $this->model->get();

		$this->data['shipment_status_log'] 	= Shipment_status_log::join('shipment_status', 'shipment_status.id', 'shipment_status_log.shipment_status_id')->join('user', 'user.id', 'shipment_status_log.user_id')->select('shipment_status_log.*', 'shipment_status.status_name as status_name')->get();

		$this->shipment_size_log 			= Shipment_size_log::orderBy('created_at', 'desc')->get();
		$this->data['shipment_size_log'] 	= $this->shipment_size_log;
		// // dd($this->data['shipment_size_log']);

		// if(count($this->shipment_size_log) > 1){
		// 	$this->data['shipment_size_log_before'] = $this->shipment_size_log[1];
		// }

		// $volume = $this->data['shipment_size_log']->volume;
		// $this->data['long']		= ($volume != null ? explode('x',$volume)[0] : '');
		// $this->data['wide']		= ($volume != null ? explode('x',$volume)[1] : '');
		// $this->data['height']	= ($volume != null ? explode('x',$volume)[2] : '');

		$this->data['city'] 				= City::join('station', 'station.city_id', 'city.id')->select('city.*')->get();
		$this->data['station'] 				= Station::where('status', 'y')->get();
		$this->data['service_delivery'] 	= Service_delivery::where('status', 'y')->get();
		$this->data['driver'] 				= User::join('station', 'station.id', 'user.station_id')->where('user_access_id', 3)->select('user.*', 'station.station_name')->get();
		$this->data['token']				= $this->generate_token();
		return $this->render_view('pages.shipment.index');
	}

	public function create(){
		$this->data['title'] 	= 'Create New '.$this->title;
		$city_exist 			= $this->model->where('city_id', '!=', null)->get();
		
		$arr = [];
		if($city_exist){
			foreach ($city_exist as $key => $value) {
				array_push($arr, $value->city_id);
			}
		}

		$this->data['city'] 	= City::whereNotIn('id', $arr)->get();
		return $this->render_view('pages.delivery_price.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request){
		$this->validate($request,[
			'station_name'			=> 'required|unique:station',
			'address'				=> 'required',
			'picture' 				=> 'mimes:jpeg,png,jpg,gif'
		]);

		$this->model->station_name 		= $request->station_name;
		$this->model->address 			= $request->address;
		$this->model->phone 			= $request->phone;
		$this->model->city_id 			= $request->city;
		$this->model->status 			= 'y';

		if ($request->hasFile('images')){
        	// File::delete($this->image_path.$this->model->picture);
			$data = [
						'name' => 'images',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}
		
		$this->model->upd_by			= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully create user');
		return redirect()->to($this->data['path']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->data['data'] = $this->model->leftJoin('agent', 'agent.id', 'shipment.agent_id')->join('shipment_status', 'shipment_status.id', 'shipment.shipment_status_id')->select('shipment.*', 'agent.business_name as agent_name', 'shipment_status.status_name as status_name')->find($id);
		$this->data['title'] 		= 'View '.$this->title.' '.$this->data['data']->shipment_no;

		return $this->render_view('pages.station.view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->data['data'] = $this->model->leftJoin('agent', 'agent.id', 'shipment.agent_id')->join('shipment_status', 'shipment_status.id', 'shipment.shipment_status_id')->select('shipment.*', 'agent.business_name as agent_name', 'shipment_status.status_name as status_name')->find($id);
		// dd($this->data['data']);

		$this->data['shipment_status_log'] 	= Shipment_status_log::where('shipment_id', $this->data['data']->id)->join('shipment_status', 'shipment_status.id', 'shipment_status_log.shipment_status_id')->join('user', 'user.id', 'shipment_status_log.user_id')->select('shipment_status_log.*', 'shipment_status.status_name as status_name')->get();

		$this->shipment_size_log 	= Shipment_size_log::where('shipment_id', $this->data['data']->id)->orderBy('updated_at', 'desc');
		$this->shipment_size_log2 	= Shipment_size_log::where('shipment_id', $this->data['data']->id)->orderBy('updated_at', 'desc');
		// dd(count($this->shipment_size_log->where('approve', 1)->get()));

		if(count($this->shipment_size_log->where('approve', 1)->get()) > 0){
			$this->shipment_size_log = $this->shipment_size_log->where('approve', 1)->get();
			$this->data['shipment_size_log'] 	= $this->shipment_size_log[0];
		}

		if(count($this->shipment_size_log2->where('approve', 0)->get()) > 0){
			$this->shipment_size_log2 = $this->shipment_size_log2->where('approve', 0)->get();
			$this->data['shipment_size_log_before'] = $this->shipment_size_log2[0];
		}

		$volume = null;
		if(isset($this->data['shipment_size_log'])){
			$volume = $this->data['shipment_size_log']->volume;
		}

		$this->data['long']		= ($volume != null ? explode('x',$volume)[0] : '');
		$this->data['wide']		= ($volume != null ? explode('x',$volume)[1] : '');
		$this->data['height']	= ($volume != null ? explode('x',$volume)[2] : '');

		$this->data['city'] 				= City::join('station', 'station.city_id', 'city.id')->select('city.*')->get();

		$this->data['station'] 				= Station::where('status', 'y')->get();

		$this->data['service_delivery'] 	= Service_delivery::where('status', 'y')->get();
		$this->data['driver'] 				= User::where('user_access_id', 3)->get();

		$this->data['title'] 				= 'Edit '.$this->title.' '.$this->data['data']->shipment_no;
		
		return $this->render_view('pages.shipment.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id){
		$this->validate($request,[
			'start_district'			=> 'required',
			'dest_district'				=> 'required',
			// 'sender_hp'				=> 'required',
			// 'images' 				=> 'mimes:jpeg,png,jpg,gif'
		]);
		
		$this->model 				= $this->model->find($id);
		// dd($this->model->shipment_status_id);		
		// if($this->model->shipment_status_id == 1){
		// 	$shipment_id 			= $this->model->id;
		// 	$user_id 				= auth()->guard($this->guard)->user()->id; //user yg melakukan action
		// 	$shipment_status_id 	= 2;
		// 	$description 			= '';
		// 	$active 				= 0;
		// 	// $this->updateShipmentStatusLog($id, $active);
		// 	// $this->insertShipmentStatusLog($shipment_id, $user_id, $shipment_status_id, $description, $active);



		// 	// $pushn 			= User::where('user_access_id', 3)->where('id', $request->user_id)->first();
		// 	// $push_data  	= [];
		// 	// $push_data2  	= '';
		// 	// $push_data3  	= [];

		// 	// $push_data[] = $pushn->token;
		// 	// if(sizeof($push_data)){
		// 	// 	Pushnotification::title('Anda Mendapatkan Tugas');
		// 	// 	Pushnotification::message('Mengambil barang dari: '.$request->sender_name);
		// 	// 	Pushnotification::registrationsid($push_data);
		// 	// 	Pushnotification::send();
		// 	// }
		// }elseif($this->model->shipment_status_id == 5){
		// 	// update current shipment size_log
		// 	$this->updateShipmentSizeLog($this->model->id);

		// 	// insert new shipment_size_log
		// 	$shipment_id 			= $this->model->id;
		// 	$weight 				= $request->weight;
		// 	$volume					= $request->long.'x'.$request->wide.'x'.$request->height; 
		// 	$total_volume 			= $request->long * $request->wide * $request->height;
		// 	$approve 				= 1;

		// 	$this->insertShipmentSizeLog($shipment_id, $weight, $volume, $total_volume, $approve);
		// }
		// elseif($this->model->shipment_status_id == '2'){

		// }
		// $this->model->shipping_no 			= $request->shipping_no;
		$start_station_id = '';
		$dest_station_id  = '';

		$station = Station::where('city_id',$request->start_district)->get();
		if(count($station) > 0){
			$start_station_id = $station[0]['id'];
		}

		$station2 = Station::where('city_id',$request->dest_district)->get();
		if(count($station2) > 0){
			$dest_station_id = $station2[0]['id'];
		}

		$this->model->start_station_id 		= $start_station_id;
		$this->model->dest_station_id 		= $dest_station_id;
		$this->model->start_address 		= $request->sender_address;
		$this->model->dest_address 			= $request->dest_address;
		$this->model->start_district 		= $request->start_district;
		$this->model->dest_district 		= $request->dest_district;
		// $this->model->shipment_status_id 	= 2;
		// $this->model->agent_id 				= $request->agent_id;

		// $this->model->city_id 				= $request->start_district;

		// $flagAssignDriver 					= 'n';
		// $this->model->badge 				= $flagAssignDriver;
		$this->model->sender_name 			= $request->sender_name;
		$this->model->receipt_name 			= $request->receipt_name;
		$this->model->sender_hp 			= $request->sender_hp;
		$this->model->service_delivery_id 	= $request->service_delivery;
		$this->model->receipt_hp 			= $request->receipt_hp;
		// $this->model->description 			= $request->description;
		$this->model->number_of_deposit 	= $request->number_of_deposit;
		
		($request->insurance == 'y' ? $this->model->asurance = 'y' : $this->model->asurance = 'n');
		($request->packing == 'y' ? $this->model->packing = 'y' : $this->model->packing = 'n');
		$this->model->latitude 				= $request->latitude;
		$this->model->longitude 			= $request->longitude;
		$this->model->sender_no_ktp 		= $request->sender_no_ktp;
		

		// if($request->input('remove-single-image-images') == 'y'){
		// 	if($this->model->images != null){
		// 		File::delete($this->image_path.$this->model->images);
		// 		$this->model->images = '';
		// 	}
		// }

		// if ($request->hasFile('images')){
		// 	$data = [
		// 				'name' => 'images',
		// 				'file_opt' => ['path' => $this->image_path]
		// 			];
		// 	$image = $this->build_image($data);
		// 	$this->model->images = $image;
		// }
		
		$this->model->upd_by			= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		// if($this->model->agent_id){
		// 	$shipment = $this->model->join('shipment_status', 'shipment_status.id', 'shipment.shipment_status_id')->select('shipment.*', 'shipment_status.status_name as status_name')->find($id);
		// 	$agent = User::join('useraccess', 'useraccess.id', 'user.user_access_id')->where('user.id', $this->model->agent_id)->select('user.*','useraccess.access_name as useraccess_name')->first();

		// 	if($agent->email){
		// 		$this->data['name'] 			= $agent->name;
		// 		$this->data['position']			= $agent->useraccess_name;
		// 		$this->data['shipment_no']		= $this->model->shipping_no;
		// 		$this->data['shipment_status']	= $this->model->shipping_no;

		// 		// dd($this->data);
		// 		//send an email
		// 		Email::to($agent->email);
		//   		Email::subject("Shipment Status");
		//   		Email::view($this->view_path.'.emails.shipment');
		//   		Email::email_data($this->data);
		//   		// Email::attach('components/files/UserDetail-'.$user->id.'.pdf');
		//   		// Email::send();
		// 	}
		// }

		Alert::success('Successfully update user');
		return redirect()->to($this->data['path']);
	}

	public function insertShipmentSizeLog($id, $weight, $volume, $total_volume, $approve){
		// insert new shipment_size_log
		$this->shipment_size_log->shipment_id 		= $id;
		$this->shipment_size_log->weight 			= $weight;
		$this->shipment_size_log->volume			= $volume; 
		$this->shipment_size_log->total_volume 		= $total_volume;
		$this->shipment_size_log->approve 			= $approve;
		// dd($this->shipment_status_log);
		$this->shipment_size_log->save();
	} 

	public function insertShipmentStatusLog($id, $user_id, $shipment_status_id, $description, $active){
		// insert new shipment_size_log
		$this->shipment_status_log->shipment_id 		= $id;
		$this->shipment_status_log->user_id 			= $user_id;
		$this->shipment_status_log->shipment_status_id 	= $shipment_status_id; 
		$this->shipment_status_log->description 		= $description;
		$this->shipment_status_log->active 				= $active;
		// dd($this->shipment_status_log);
		$this->shipment_status_log->save();
	} 

	public function updateShipmentSizeLog($id, $approve, $order = 'asc'){
		// update shipment_size_log
		$shipment_size_log 						= Shipment_size_log::where('shipment_id', $id)->orderBy('created_at',$order)->first();
		$shipment_size_log->approve 			= $approve;
		$shipment_size_log->save();
	}

	public function updateShipmentSizeLogBefore($id, $approve){
		// update shipment_size_log
		$shipment_size_log 						= Shipment_size_log::where('shipment_id', $id)->where('approve', 0)->orderBy('created_at','desc')->first();
		$shipment_size_log->approve 			= $approve;
		$shipment_size_log->save();
	}

	public function updateShipmentStatusLog($id, $active){
		// update shipment_status_log
		$shipment_status_log 					= Shipment_status_log::where('shipment_id', $id)->orderBy('created_at','desc')->first();
		$shipment_status_log->active 			= $active;
		// dd($this->shipment_status_log);
		$shipment_status_log->save();
	} 

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request){
		$id 		= $request->id;
		$uc 		= $this->model->find($id);
		$store 		= Station::where('id', $uc->id);
		if($uc->images != null){
			File::delete($this->image_path.$uc->images);
			$uc->images = '';
		}

		$uc->delete();
		
		Alert::success($this->title.' '.$uc->station_name.' has been deleted');
		return redirect()->back();
	}

	public function cancel_shipment(Request $request){
		$id 		= $request->id;
		$uc 		= $this->model->find($id);
		$shipment 	= Shipment::find($id);
		$shipment->Shipment_status = 21;
		$shipment->save();
		
		Alert::success($this->title.' '.$uc->shipping_no.' has been updates');
		return redirect()->back();
	}

	public function ext(request $request, $action){
		return $this->$action($request);
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function get_user(){
		return 1;
		$q = User::where('id', '!=',null)->get();
		return $q;
	}

	public function get_language(){
		$q = Mslanguage::where('status','y')->orderBy('order','asc')->pluck('language_name','id')->toArray();
		return $q;
	}

	public function export(){
		if(in_array(auth()->guard($this->guard)->user()->store_id,["0","1"])){
			$users = '';
		}else{
			$users = $this->get_userId_byStore();
		}
		return $this->build_export($users);
	}

	public function exporting(){
		// dd($request->from_city);
		// dd(json_encode($arr));
		// if($request->from_city == ''){
		// 	$request->from_city = 0;
		// 	// return 'From City Required';
		// }elseif($request->to_city == ''){
		// 	$request->to_city = 0;
		// 	// return 'To City Required';
		// }

		$query 					= City::whereIn('id', $this->formatTitikKomaToArray($this->city_id_allowed->value))->orderBy('city.province_id', 'asc');
		$query2 				= City::whereIn('id', $this->formatTitikKomaToArray($this->city_id_allowed->value))->orderBy('city.province_id', 'asc');
		$city_price_delivery  	= $this->model->get();
		$service_delivery  		= Service_delivery::where('status', 'y')->get();
		$city_delivery_price 	= City_delivery_price::get();

		// // in_array if find 0 value, all city
		// if($request->from_city != "" && !in_array(0, $request->from_city)){
		// 	$query->whereIn('id', $request->from_city);
		// }

		// if($request->to_city != "" && !in_array(0, $request->to_city)){
		// 	$query2->whereIn('id', $request->to_city);
		// }

		$display = "";
		$get_last = "";

		$city 	= $query->get();
		$city2 	= $query2->get();

		$countServiceDelivery = count($service_delivery);
		$display .='<div class="table-scrollable">
		            <table id="table-laporan" class="table table-hover table-light">
		              <thead>
		                <tr>
		                  <th class="bg-blue-madison font-white">city_from</th>
		                  <th class="bg-blue-madison font-white">from_city_id</th>
		                  <th class="bg-blue-madison font-white">city_to</th>
		                  <th class="bg-blue-madison font-white">to_city_id</th>';

        foreach ($service_delivery as $sd) {
			$display .= '<th class="bg-blue-madison font-white">weight-'.$sd->id.'-'.strtolower(str_replace(" ", "_", $sd->service_delivery_name)).'</th>';
		}

		foreach ($service_delivery as $sd) {
			$display .= '<th class="bg-blue-madison font-white">volume-'.$sd->id.'-'.strtolower(str_replace(" ", "_", $sd->service_delivery_name)).'</th>';
		}
		
		$display .=   '</tr>
		              </thead>
		              <tbody>';

		
		$city_arr 		= [];
		$city_arr2 		= [];
		$city_count_arr = [];
		foreach($city as $c){
			$display2		= '';

			array_push($city_arr, $c->id);
			array_push($city_count_arr, $c->id);
			$flagrow = 0;
			foreach($city2 as $c2){
				if(!in_array($c2->id, $city_arr) && !in_array($c2->id.'-'. $c->id, $city_arr2)){

					if($flagrow == 0){
						$display2 .= '<tr id='.$c->id.'>';
						$display2 .= '<td>'.$c->name.'</td>';
						$display2 .= '<td>'.$c->id.'</td>';
						$display2 .= '<td>'.$c2->name.'</td>';
						$display2 .= '<td>'.$c2->id.'</td>';
						$weight 	= '';
						$volume 	= '';
						foreach ($service_delivery as $sd) {
							foreach ($city_delivery_price as $key => $cdp){
								if($cdp->from_city_id == $c->id && $cdp->to_city_id == $c2->id){
									$weight_price =	json_decode($cdp->weight_price);
									if(count($weight_price) > 0){
										foreach ($weight_price as $wp) {
											if($wp->service_delivery_id == $sd->id){
												$price 		= $wp->price;
												$weight 	.= '<td>'.$price.'</td>';
											}
										}
									}
									
									$volume_price =	json_decode($cdp->volume_price);
									if(count($volume_price) > 0){
										foreach ($volume_price as $vp){
											if($vp->service_delivery_id == $sd->id){
												$price3 	= $vp->price;
												$volume 	.= '<td>'.$price3.'</td>';
											}
										}
									}
								}
							}
							if($weight == ''){
								$weight .= '<td></td><td></td>';
							}

							if($volume == ''){
								$volume .= '<td></td><td></td>';
							}
						}
						$display2 .= $weight.$volume;
						$display2 .= '</tr>';
						$flagrow = 1;
					}else{
						$display2 .= '<tr id='.$c->id.'>';
						$display2 .= '<td>'.$c->name.'</td>';
						$display2 .= '<td>'.$c->id.'</td>';
						$display2 .= '<td>'.$c2->name.'</td>';
						$display2 .= '<td>'.$c2->id.'</td>';
						$weight 	= '';
						$volume 	= '';
						foreach ($service_delivery as $sd) {
							foreach ($city_delivery_price as $key => $cdp){
								if($cdp->from_city_id == $c->id && $cdp->to_city_id == $c2->id){
									$weight_price =	json_decode($cdp->weight_price);
									if(count($weight_price) > 0){
										foreach ($weight_price as $wp) {
											if($wp->service_delivery_id == $sd->id){
												$price = $wp->price;
												$weight 	.= '<td>'.$price.'</td>';
											}
										}
									}

									$volume_price =	json_decode($cdp->volume_price);
									if(count($volume_price) > 0){
										foreach ($volume_price as $vp){
											if($vp->service_delivery_id == $sd->id){
												$price3 = $vp->price;
												$volume 	.= '<td>'.$price3.'</td>';
											}
										}
									}
								}
							}
							if($weight == ''){
								$weight .= '<td></td><td></td>';
							}

							if($volume == ''){
								$volume .= '<td></td><td></td>';
							}
						}
						$display2 .= $weight.$volume;
						$display2 .= '</tr>';
						
					}
					array_push($city_arr2, $c->id.'-'. $c2->id);
					array_push($city_arr2, $c2->id.'-'. $c->id);
					array_push($city_count_arr, $c2->id);
				}
			}

			$flagStop = 0;
			$vals = array_count_values($city_count_arr);
			if(count($vals) > 0){
				
				if(isset($vals[$c->id])){
					// if count array_count_values equal to $city
					if($vals[$c->id] == count($city) && count($city) != 1){
						// $flagStop = 1;
					}
				}
			}
			if($flagStop == 0){
				$display .= $display2;
			}

		}
		
		$display .='</tbody>
					</table>
					</div>';
		// dd($display);			
		Report::setdata($display);
		Report::settitle('Delivery Price List');
		Report::setview('admin.builder.excel2');
		Report::settype('excel');
		Report::setformat('xlsx');
		Report::setcreator('paketin');
		Report::generate();
	}

	public function change_status($request){
		if($request->shipment_status != 'cancel' && $request->shipment_status == 9 && $request->shipment_status == 10){
			if($request->start_district == ''){
				return 'Start District name must be filled !';
			}elseif($request->dest_district == ''){
				return 'Destination District name must be filled !';
			}
		}
		
		$this->model 				= $this->model->find($request->id_shipment);
		$flagAssignDriver = '';
		if($request->shipment_status == 1 || $request->shipment_status == 11){
			if($request->user_id == ''){
				return 'Driver name must be filled !';
			}

			$shipment_id 				= $this->model->id;
			$user_id 					= $request->user_id; //user yg melakukan action
			($request->shipment_status == 1 ? $shipment_status_id = 2 : $shipment_status_id = 12);
			$description 				= '';
			$flagAssignDriver 			= 'y';

			// insert new shipment size_log
			$this->insertShipmentStatusLog($shipment_id, $user_id, $shipment_status_id, $description, 1);
		}elseif($request->shipment_status == 5){
			// update current shipment size_log
			$this->updateShipmentSizeLog($this->model->id, 0);
			$this->updateShipmentSizeLog($this->model->id, 1, 'desc');

			// insert new shipment_size_log
			$shipment_id 				= $this->model->id;
			$shipment_status_id 		= 6;
			// $weight 				= $request->weight;
			// $volume					= $request->volumet; 
			// $total_volume 			= $request->long * $request->wide * $request->height;
			// $approve 				= 1;

			// $this->insertShipmentSizeLog($shipment_id, $weight, $volume, $total_volume, $approve);
			$shipment_id 				= $this->model->id;
			$driver 					= Shipment_status_log::where('shipment_id', $shipment_id)->where('active', 1)->first();
			$user_id 					= $driver->user_id; //user yg melakukan action
			$description 				= '';

			// $this->updateShipmentStatusLog($shipment_id, 0);
			// $this->insertShipmentStatusLog($shipment_id, $user_id, $shipment_status_id, $description, 1);
		}elseif($request->shipment_status == 8){
			// insert new shipment_size_log
			$shipment_id 				= $this->model->id;
			$shipment_status_id 		= 9;
			$shipment_id 				= $this->model->id;
			$driver 					= Shipment_status_log::where('shipment_id', $shipment_id)->where('active', 1)->first();
			$user_id 					= $driver->user_id; //user yg melakukan action
			$description 				= '';

			$this->insertShipmentStatusLog($shipment_id, $user_id, $shipment_status_id, $description, 1);
		}elseif($request->shipment_status == 9 || $request->shipment_status == 10){
			$shipment_id 				= $this->model->id;
			$shipment_size_log 			= Shipment_status_log::where('shipment_id', $this->model->id)->orderBy('created_at','desc')->first();
			// return $shipment_size_log;
			$user_id 					= $shipment_size_log->user_id; //user yg melakukan action
			($request->shipment_status == 9 ? $shipment_status_id = 10 : $shipment_status_id = 11);
			$description 				= '';
			// $flagAssignDriver 			= 'y';

			// insert new shipment size_log
			$this->insertShipmentStatusLog($shipment_id, $user_id, $shipment_status_id, $description, 1);
		}


		if($request->shipment_status == 'cancel'){
			$shipment_id 				= $this->model->id;
			$shipment_status_id 		= 21;
		}

		if($request->shipment_status == 'reject'){
			$shipment_id 				= $this->model->id;
			$shipment_status_id 		= 21;

			$driver 					= Shipment_status_log::where('shipment_id', $shipment_id)->where('active', 1)->first();
			$user_id 					= $driver->user_id; //user yg melakukan action
			$description 				= '';


			$this->updateShipmentStatusLog($shipment_id, 0);
		}


		$dest_station_id  = '';

		$station = Station::where('city_id',$request->start_district)->get();
		if(count($station) > 0){
			$start_station_id = $station[0]['id'];
		}

		$station2 = Station::where('city_id',$request->dest_district)->get();
		if(count($station2) > 0){
			$dest_station_id = $station2[0]['id'];
		}

		($start_station_id != '' ? $this->model->start_station_id = $start_station_id : '');
		($dest_station_id != '' ? $this->model->dest_station_id = $dest_station_id : '');
		// $this->model->start_address 		= $request->sender_address;
		// $this->model->dest_address 			= $request->dest_address;
		$this->model->start_district 		= $request->start_district;
		$this->model->dest_district 		= $request->dest_district;
		$this->model->shipment_status_id 	= $shipment_status_id;
		// $this->model->agent_id 				= $request->agent_id;

		// $this->model->city_id 				= $request->start_district;

		
		($flagAssignDriver != '' ? $this->model->badge = $flagAssignDriver : '');
		// $this->model->sender_name 			= $request->sender_name;
		// $this->model->receipt_name 			= $request->receipt_name;
		// $this->model->sender_hp 			= $request->sender_hp;
		// $this->model->service_delivery_id 	= $request->service_delivery;
		// $this->model->receipt_hp 			= $request->receipt_hp;
		// $this->model->description 			= $request->description;
		// $this->model->number_of_deposit 	= $request->number_of_deposit;
		
		// ($request->insurance == 'y' ? $this->model->asurance = 'y' : $this->model->asurance = 'n');
		// ($request->packing == 'y' ? $this->model->packing = 'y' : $this->model->packing = 'n');
		// $this->model->latitude 				= $request->latitude;
		// $this->model->longitude 			= $request->longitude;
		// $this->model->sender_no_ktp 		= $request->sender_no_ktp;
		

		// if($request->input('remove-single-image-images') == 'y'){
		// 	if($this->model->images != null){
		// 		File::delete($this->image_path.$this->model->images);
		// 		$this->model->images = '';
		// 	}
		// }

		// if ($request->hasFile('images')){
		// 	$data = [
		// 				'name' => 'images',
		// 				'file_opt' => ['path' => $this->image_path]
		// 			];
		// 	$image = $this->build_image($data);
		// 	$this->model->images = $image;
		// }
		
		$this->model->upd_by			= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		if($this->model->agent_id){
			// $shipment = $this->model->join('shipment_status', 'shipment_status.id', 'shipment.shipment_status_id')->select('shipment.*', 'shipment_status.status_name as status_name')->find($id);
			$agent 				= Agent::where('id', $this->model->agent_id)->first();
			$status_shipping 	= Shipment_status::where('id', $shipment_status_id)->first();
			if($agent->email){
				$this->data['name'] 			= $agent->business_cp_full_name_title;
				$this->data['business_name']	= $agent->business_name;
				$this->data['shipment_no']		= $this->model->shipping_no;
				$this->data['shipment_status']	= $status_shipping->status_name;
				// dd($this->data);
				//send an email
				Email::to($agent->email);
		  		Email::subject("Shipment Status");
		  		Email::view($this->view_path.'.emails.shipment');
		  		Email::email_data($this->data);
		  		// Email::attach('components/files/UserDetail-'.$user->id.'.pdf');
		  		Email::send();
			}
			return '1';
		}
	}

	public function filter($request){
		// dd($request->from_city);
		// dd(json_encode($arr));
		if($request->from_city == ''){
			$request->from_city = 0;
			// return 'From City Required';
		}elseif($request->to_city == ''){
			$request->to_city = 0;
			// return 'To City Required';
		}

		$query 					= City::whereIn('id', $this->formatTitikKomaToArray($this->city_id_allowed->value))->orderBy('city.province_id', 'asc');
		$query2 					= City::whereIn('id', $this->formatTitikKomaToArray($this->city_id_allowed->value))->orderBy('city.province_id', 'asc');
		$city_price_delivery  	= $this->model->get();
		$service_delivery  		= Service_delivery::where('status', 'y')->get();
		$city_delivery_price 	= City_delivery_price::get();

		// in_array if find 0 value, all city
		if($request->from_city != "" && !in_array(0, $request->from_city)){
			$query->whereIn('id', $request->from_city);
		}

		if($request->to_city != "" && !in_array(0, $request->to_city)){
			$query2->whereIn('id', $request->to_city);
		}

		$display = "";
		$get_last = "";

		$city 	= $query->get();
		$city2 	= $query2->get();

		$countServiceDelivery = count($service_delivery);
		$display .='<div class="table-scrollable">
		            <table id="table-laporan" class="table table-hover table-light">
		              <thead>
		                <tr>
		                  <th class="bg-blue-madison font-white">City / District From</th>
		                  <th class="bg-blue-madison font-white" style="display:none;">Id From</th>
		                  <th class="bg-blue-madison font-white">City / District To</th>
		                  <th class="bg-blue-madison font-white" style="display:none;">Id To</th>
		                  <th class="bg-blue-madison font-white" colspan="'.$countServiceDelivery.'" style="text-align:center;">Weight</th>
		                  <th class="bg-blue-madison font-white" colspan="'.$countServiceDelivery.'" style="text-align:center;">Volume</th>
		                </tr>
		              </thead>
		              <tbody>';

		$display .= '<tr><td class="bg-blue-madison font-white"></td><td class="bg-blue-madison font-white"></td><td class="bg-blue-madison font-white" style="display:none;"></td><td class="bg-blue-madison font-white" style="display:none;"></td>';
		
		foreach ($service_delivery as $sd) {
			$display .= '<td class="bg-blue-madison font-white">'.$sd->service_delivery_name.'</td>';
		}

		foreach ($service_delivery as $sd) {
			$display .= '<td class="bg-blue-madison font-white">'.$sd->service_delivery_name.'</td>';
		}
		$display .= '</tr>';

		
		$city_arr 		= [];
		$city_arr2 		= [];
		$city_count_arr = [];
		foreach($city as $c){
			$display2		= '';

			array_push($city_arr, $c->id);
			array_push($city_count_arr, $c->id);
			// flag if 1 = new row
			$flagrow = 0;
			foreach($city2 as $c2){
				if(!in_array($c2->id, $city_arr) && !in_array($c2->id.'-'. $c->id, $city_arr2)){

					if($flagrow == 0){
						$display2 .= '<tr id='.$c->id.'>';
						$display2 .= '<td>'.$c->name.'</td>';
						$display2 .= '<td style="display:none;">'.$c->id.'</td>';
						$display2 .= '<td>'.$c2->name.'</td>';
						$display2 .= '<td style="display:none;">'.$c2->id.'</td>';
						$weight 	= '';
						$volume 	= '';
						foreach ($service_delivery as $sd) {
							foreach ($city_delivery_price as $key => $cdp){
								if($cdp->from_city_id == $c->id && $cdp->to_city_id == $c2->id){
									$weight_price =	json_decode($cdp->weight_price);
									if(count($weight_price) > 0){
										foreach ($weight_price as $wp) {
											if($wp->service_delivery_id == $sd->id){
												$price 		= $wp->price;
												$weight 	.= '<td>'.$price.'</td>';
											}
										}
									}
									
									$volume_price =	json_decode($cdp->volume_price);
									if(count($volume_price) > 0){
										foreach ($volume_price as $vp){
											if($vp->service_delivery_id == $sd->id){
												$price3 	= $vp->price;
												$volume 	.= '<td>'.$price3.'</td>';
											}
										}
									}
								}
							}
							if($weight == ''){
								$weight .= '<td></td><td></td>';
							}

							if($volume == ''){
								$volume .= '<td></td><td></td>';
							}
						}
						$display2 .= $weight.$volume;
						$display2 .= '</tr>';
						$flagrow = 1;
					}else{
						$display2 .= '<tr id='.$c->id.'>';
						$display2 .= '<td>'.$c->name.'</td>';
						$display2 .= '<td style="display:none;">'.$c->id.'</td>';
						$display2 .= '<td>'.$c2->name.'</td>';
						$display2 .= '<td style="display:none;">'.$c2->id.'</td>';
						$weight 	= '';
						$volume 	= '';
						foreach ($service_delivery as $sd) {	
							foreach ($city_delivery_price as $key => $cdp){
								if($cdp->from_city_id == $c->id && $cdp->to_city_id == $c2->id){
									$weight_price =	json_decode($cdp->weight_price);
									if(count($weight_price) > 0){
										foreach ($weight_price as $wp) {
											if($wp->service_delivery_id == $sd->id){
												$price = $wp->price;
												$weight 	.= '<td>'.$price.'</td>';
											}
										}
									}

									$volume_price =	json_decode($cdp->volume_price);
									if(count($volume_price) > 0){
										foreach ($volume_price as $vp){
											if($vp->service_delivery_id == $sd->id){
												$price3 = $vp->price;
												$volume 	.= '<td>'.$price3.'</td>';
											}
										}
									}
								}
							}

							if($weight == ''){
								$weight .= '<td></td><td></td>';
							}

							if($volume == ''){
								$volume .= '<td></td><td></td>';
							}
						}
						$display2 .= $weight.$volume;
						$display2 .= '</tr>';
						
					}
					array_push($city_arr2, $c->id.'-'. $c2->id);
					array_push($city_arr2, $c2->id.'-'. $c->id);
					array_push($city_count_arr, $c2->id);
				}
			}

			$flagStop = 0;
			$vals = array_count_values($city_count_arr);
			if(count($vals) > 0){
				
				if(isset($vals[$c->id])){
					// if count array_count_values equal to $city
					if($vals[$c->id] == count($city) && count($city) != 1){
						// $flagStop = 1;
					}
				}
			}
			if($flagStop == 0){
				$display .= $display2;
			}

		}
		
		$display .='</tbody>
					</table>
					</div>';
		return $display;
	}

	function getCityPriceDelivery($id, $id2){
		$city_delivery_price 	= City_delivery_price::get();
		$price = "";
		foreach ($city_delivery_price as $key => $cdp){
			if($cdp->from_city_id == $id && $cdp->to_city_id == $id2){
				$weight_price =	json_decode($cdp->weight_price);
				if(count($weight_price) > 0){
					foreach ($weight_price as $wp) {
						if($wp->service_delivery_id == $sd->id){
							$price = $wp->price;
							return $price;
						}
					}
				}
			}
		}

		return $price;
	}

	public function postFiles(request $request){
		// if($request->action == 'import'){
		// dd('test');
			// return 'postFiles';

			if(!$request->hasFile('fileToUpload')){
				Alert::fail('Please upload excel file');
				return redirect()->back();
			}

			$excel 					= $request->file('fileToUpload');
			$status 				= true;
			$error_row  			= [];

			Excel::load($excel, function($reader) use(&$status,&$error_row) {
				if($reader->get()->count() == 0){
					$status = false;
					return;
				}
				$all 					= $reader->get()->toArray();
				$firstrow 				= $reader->first()->toArray();
				$service_delivery  		= Service_delivery::where('status', 'y')->get();

				// $head 		= ['username', 'password', 'email', 'user_group', 'timetable_creation_template', 'use_last_clock_in_photo', 
				// 				'location', 'activate_account', 'barcode_verified', 'force_to_log_out', 'out_of_location_alert', 'allow_overtime', 'work_on_holiday', 'first_name', 'last_name', 'mobile_phone'];

				// foreach($firstrow as $fr => $fq){
				// 	if(!in_array($fr,$head)){
				// 		$status 	= false;
				// 	}
				// }



				// if($status == true){
					$row_data 		= 0;
					$temp_username 	= [];
					$temp_arr 		= [];
					$temp_email 	= [];
					$weight_price 	= [];
					$volume_price 	= [];
					$error_row 		= [];
					// dd($all);
					foreach($all as $a){
						$row_data++;
						$flag_required = 0;
						if(empty($a['from_city_id']) || empty($a['to_city_id']) || empty($a['city_from']) || empty($a['city_to'])){
							dd($a);
							$error_row[$row_data]['required'] = 'true';
							$flag_required = 1;
						}

						$city_from_exist 	= City::where('id', $a['from_city_id'])->get();
						$city_to_exist 		= City::where('id', $a['to_city_id'])->get();
						if(count($city_from_exist) == 0 ||  count($city_to_exist) == 0){
							$city_from_exist2 	= count($city_from_exist) == 0 ? $a['from_city_id'].';' : '';
							$city_to_exist2 	= count($city_to_exist) == 0 ? $a['from_city_to'].';' : '';
							$error_row[$row_data]['citynotfound'] = 'true';
							$error_row[$row_data]['citynotfound_value'] = $city_from_exist2.$city_to_exist2;
						}

						if($flag_required == 0){
							foreach ($service_delivery as $sd) {
								if(!empty($a['weight_'.$sd['id'].'_'.strtolower(str_replace(" ", "_", $sd->service_delivery_name))])){
									$temp1 = [
										"service_delivery_id" 	=> $sd->id,
										"service_delivery_name"	=> $sd->service_delivery_name,
										"price"					=> $a['weight_'.$sd->id.'_'.strtolower(str_replace(" ", "_", $sd->service_delivery_name))]
									];

									array_push($weight_price, $temp1);
								}else if(empty($a['weight_'.$sd->id.'_'.strtolower(str_replace(" ", "_", $sd->service_delivery_name))]) && $flag_required == 0){
									$error_row[$row_data]['required'] = 'true';
									$flag_required = 1;
								}

								if(!empty($a['volume_'.$sd['id'].'_'.strtolower(str_replace(" ", "_", $sd->service_delivery_name))])){
									$temp1 = [
										"service_delivery_id" 	=> $sd->id,
										"service_delivery_name"	=> $sd->service_delivery_name,
										"price"					=> $a['volume_'.$sd->id.'_'.strtolower(str_replace(" ", "_", $sd->service_delivery_name))]
									];

									array_push($volume_price, $temp1);
								}else if(empty($a['volume_'.$sd->id.'_'.strtolower(str_replace(" ", "_", $sd->service_delivery_name))]) && $flag_required == 0){
									$error_row[$row_data]['required'] = 'true';
									$flag_required = 1;
								}
							}
							
							if(count($temp_arr) > 0){
								$flagExistData = 0;
								foreach($temp_arr as $key => $ta){
									if($ta['from_city_id'] == $a['from_city_id'] && $ta['to_city_id'] == $a['to_city_id']){
										$flagExistData 			= 1;
										// dd($a);
										$temp_arr[$key]['from_city_id'] 	= $a['from_city_id'];
										$temp_arr[$key]['to_city_id'] 		= $a['to_city_id'];
										$temp_arr[$key]['from_city_name'] 	= $a['city_from'];
										$temp_arr[$key]['to_city_name'] 	= $a['city_to'];
										$temp_arr[$key]['weight_price'] 	= json_encode($weight_price);
										$temp_arr[$key]['volume_price'] 	= json_encode($volume_price);
										// dd($temp_arr);
									}
								}

								if($flagExistData == 0){
									$temp_arr[] =[
										'from_city_id' 		=> $a['from_city_id'],
										'to_city_id' 		=> $a['to_city_id'],
										'from_city_name' 	=> $a['city_from'],
										'to_city_name' 		=> $a['city_to'],
										'weight_price' 		=> json_encode($weight_price),
										'volume_price' 		=> json_encode($volume_price),
									]; 
								}
							}else{
								$temp_arr[] =[
									'from_city_id' 		=> $a['from_city_id'],
									'to_city_id' 		=> $a['to_city_id'],
									'from_city_name' 	=> $a['city_from'],
									'to_city_name' 		=> $a['city_to'],
									'weight_price' 		=> json_encode($weight_price),
									'volume_price' 		=> json_encode($volume_price),
								]; 
							}
						}
						
						$weight_price 	= [];
						$volume_price 	= [];
					}
					
					// dd($error_row);
					if(count($error_row) < 1 && count($temp_arr) > 0){
						// dd($temp_arr);
						City_delivery_price::where('id', '!=' ,null)->delete();
						City_delivery_price::insert($temp_arr);
					}
				// }
			});
			if($status == false){
				Alert::fail(trans('general.file_wrong'));
			}else if(count($error_row) > 0){
				$request->session()->flash('error_row', $error_row);
				Alert::fail(trans('general.file_unvalid'));
			}else{
				Alert::success(trans('general.file_has_updated'));
			}
			return redirect()->back();
		// }
	}
}
