<?php namespace digipos\Http\Controllers\Admin;

use DB;
use Session;
use Hash;
use File;
use Excel;
use Schema;

use digipos\models\User;
use digipos\models\Station;
use digipos\models\Msmenu;
use digipos\models\Useraccess;
use digipos\models\Mslanguage;
use digipos\models\City;
use digipos\models\Shipment;
use digipos\models\Shipment_log;
use digipos\models\Shipment_size_log;
use digipos\models\City_delivery_price;
use digipos\models\Service_delivery;
use digipos\models\Config;
use digipos\Libraries\Report;

use digipos\Libraries\Alert;
use Illuminate\Http\Request;

class DeliveryPriceController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 				= "Delivery Price";
		$this->root_url				= "delivery_price";
		$this->root_link 			= "delivery_price";
		$this->model 				= new City_delivery_price;
		$this->image_path 			= 'components/both/images/delivery_price/';
		$this->image_path2 			= 'components/both/images/web/';
		$this->data['image_path'] 	= $this->image_path;
		$this->data['image_path2'] 	= $this->image_path2;
		$this->data['root_url']		= $this->root_url;

		$this->measurement = ['weight', 'volume'];
		// get city id allowed
		$this->city_id_allowed 	= Config::where('name', 'city_id')->first();
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->data['title'] 					= $this->title;
		$this->data['city'] 					= City::whereIn('id', $this->formatTitikKomaToArray($this->city_id_allowed->value))->get();
		$this->data['city_price_delivery']  	= $this->model->get();
		$this->data['service_delivery']  		= Service_delivery::where('status', 'y')->get();
		return $this->render_view('pages.delivery_price.index');
	}

	public function create(){
		// $this->data['title'] 	= 'Create New '.$this->title;
		// $city_exist 			= $this->model->where('city_id', '!=', null)->get();
		
		// $arr = [];
		// if($city_exist){
		// 	foreach ($city_exist as $key => $value) {
		// 		array_push($arr, $value->city_id);
		// 	}
		// }

		// $this->data['city'] 	= City::whereNotIn('id', $arr)->get();

		// return $this->render_view('pages.station.create');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request){
		// $this->validate($request,[
		// 	'station_name'			=> 'required|unique:station',
		// 	'address'				=> 'required',
		// 	'picture' 				=> 'mimes:jpeg,png,jpg,gif'
		// ]);

		// $this->model->station_name 		= $request->station_name;
		// $this->model->address 			= $request->address;
		// $this->model->phone 			= $request->phone;
		// $this->model->city_id 			= $request->city;
		// $this->model->status 			= 'y';

		// if ($request->hasFile('images')){
  //       	// File::delete($this->image_path.$this->model->picture);
		// 	$data = [
		// 				'name' => 'images',
		// 				'file_opt' => ['path' => $this->image_path]
		// 			];
		// 	$image = $this->build_image($data);
		// 	$this->model->images = $image;
		// }
		
		// $this->model->upd_by			= auth()->guard($this->guard)->user()->id;
		// // dd($this->model);
		// $this->model->save();

		// Alert::success('Successfully create user');
		// return redirect()->to($this->data['path']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		// $this->data['data'] 		= $this->model->join('city', 'city.id', 'station.city_id')->select('station.*', 'city.name as city_name')->find($id);
		// $this->data['title'] 		= 'View '.$this->title.' '.$this->data['data']->station_name;

		// return $this->render_view('pages.station.view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		// $this->data['data'] 		= $this->model->find($id);
		// $city_exist 				= $this->model->where('city_id', '!=', null)->get();
		// // dd($city_exist);
		// $arr = [];
		// if($city_exist){
		// 	foreach ($city_exist as $key => $value) {
		// 		array_push($arr, $value->city_id);
		// 	}
		// }
		// // dd($arr);
		// $this->data['city'] 		= City::whereNotIn('id', $arr)->get();
		// $this->data['title'] 		= 'Edit '.$this->title.' '.$this->data['data']->station_name;
		
		// return $this->render_view('pages.station.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id){
		// $this->validate($request,[
		// 	'station_name'			=> 'required|unique:station,station_name,'.$id,
		// 	'address'				=> 'required',
		// 	'picture' 				=> 'mimes:jpeg,png,jpg,gif'
		// ]);
		
		// $this->model 					= $this->model->find($id);
		// $this->model->station_name 		= $request->station_name;
		// $this->model->address 			= $request->address;
		// $this->model->phone 			= $request->phone;
		// $this->model->city_id 			= $request->city;
		// $this->model->status 			= 'y';

		// if($request->input('remove-single-image-images') == 'y'){
		// 	if($this->model->images != null){
		// 		File::delete($this->image_path.$this->model->images);
		// 		$this->model->images = '';
		// 	}
		// }

		// if ($request->hasFile('images')){
		// 	$data = [
		// 				'name' => 'images',
		// 				'file_opt' => ['path' => $this->image_path]
		// 			];
		// 	$image = $this->build_image($data);
		// 	$this->model->images = $image;
		// }
		
		// $this->model->upd_by			= auth()->guard($this->guard)->user()->id;
		// // dd($this->model);
		// $this->model->save();

		// Alert::success('Successfully update user');
		// return redirect()->to($this->data['path']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request){
		// $id 		= $request->id;
		// $uc 		= $this->model->find($id);
		// $store 		= Station::where('id', $uc->id);
		// if($uc->images != null){
		// 	File::delete($this->image_path.$uc->images);
		// 	$uc->images = '';
		// }

		// $uc->delete();
		
		// Alert::success($this->title.' '.$uc->station_name.' has been deleted');
		// return redirect()->back();
	}

	public function ext(request $request, $action){
		return $this->$action($request);
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function get_user(){
		return 1;
		$q = User::where('id', '!=',null)->get();
		return $q;
	}

	public function get_language(){
		$q = Mslanguage::where('status','y')->orderBy('order','asc')->pluck('language_name','id')->toArray();
		return $q;
	}

	public function export(){
		if(in_array(auth()->guard($this->guard)->user()->store_id,["0","1"])){
			$users = '';
		}else{
			$users = $this->get_userId_byStore();
		}
		return $this->build_export($users);
	}

	public function exporting(){
		// dd($request->from_city);
		// dd(json_encode($arr));
		// if($request->from_city == ''){
		// 	$request->from_city = 0;
		// 	// return 'From City Required';
		// }elseif($request->to_city == ''){
		// 	$request->to_city = 0;
		// 	// return 'To City Required';
		// }

		$query 					= City::whereIn('id', $this->formatTitikKomaToArray($this->city_id_allowed->value))->orderBy('city.province_id', 'asc');
		$query2 				= City::whereIn('id', $this->formatTitikKomaToArray($this->city_id_allowed->value))->orderBy('city.province_id', 'asc');
		$city_price_delivery  	= $this->model->get();
		$service_delivery  		= Service_delivery::where('status', 'y')->get();
		$city_delivery_price 	= City_delivery_price::get();

		// // in_array if find 0 value, all city
		// if($request->from_city != "" && !in_array(0, $request->from_city)){
		// 	$query->whereIn('id', $request->from_city);
		// }

		// if($request->to_city != "" && !in_array(0, $request->to_city)){
		// 	$query2->whereIn('id', $request->to_city);
		// }

		$display = "";
		$get_last = "";

		$city 	= $query->get();
		$city2 	= $query2->get();

		$countServiceDelivery = count($service_delivery);
		$display .='<div class="table-scrollable">
		            <table id="table-laporan" class="table table-hover table-light">
		              <thead>
		                <tr>
		                  <th class="bg-blue-madison font-white">city_from</th>
		                  <th class="bg-blue-madison font-white">from_city_id</th>
		                  <th class="bg-blue-madison font-white">city_to</th>
		                  <th class="bg-blue-madison font-white">to_city_id</th>';

        foreach ($service_delivery as $sd) {
			$display .= '<th class="bg-blue-madison font-white">weight-'.$sd->id.'-'.strtolower(str_replace(" ", "_", $sd->service_delivery_name)).'</th>';
		}

		foreach ($service_delivery as $sd) {
			$display .= '<th class="bg-blue-madison font-white">volume-'.$sd->id.'-'.strtolower(str_replace(" ", "_", $sd->service_delivery_name)).'</th>';
		}
		
		$display .=   '</tr>
		              </thead>
		              <tbody>';

		
		$city_arr 		= [];
		$city_arr2 		= [];
		$city_count_arr = [];
		foreach($city as $c){
			$display2		= '';

			array_push($city_arr, $c->id);
			array_push($city_count_arr, $c->id);
			$flagrow = 0;
			foreach($city2 as $c2){
				if(!in_array($c2->id, $city_arr) && !in_array($c2->id.'-'. $c->id, $city_arr2)){

					if($flagrow == 0){
						$display2 .= '<tr id='.$c->id.'>';
						$display2 .= '<td>'.$c->name.'</td>';
						$display2 .= '<td>'.$c->id.'</td>';
						$display2 .= '<td>'.$c2->name.'</td>';
						$display2 .= '<td>'.$c2->id.'</td>';
						$weight 	= '';
						$volume 	= '';
						foreach ($service_delivery as $sd) {
							foreach ($city_delivery_price as $key => $cdp){
								if($cdp->from_city_id == $c->id && $cdp->to_city_id == $c2->id){
									$weight_price =	json_decode($cdp->weight_price);
									if(count($weight_price) > 0){
										foreach ($weight_price as $wp) {
											if($wp->service_delivery_id == $sd->id){
												$price 		= $wp->price;
												$weight 	.= '<td>'.$price.'</td>';
											}
										}
									}
									
									$volume_price =	json_decode($cdp->volume_price);
									if(count($volume_price) > 0){
										foreach ($volume_price as $vp){
											if($vp->service_delivery_id == $sd->id){
												$price3 	= $vp->price;
												$volume 	.= '<td>'.$price3.'</td>';
											}
										}
									}
								}
							}
							if($weight == ''){
								$weight .= '<td></td><td></td>';
							}

							if($volume == ''){
								$volume .= '<td></td><td></td>';
							}
						}
						$display2 .= $weight.$volume;
						$display2 .= '</tr>';
						$flagrow = 1;
					}else{
						$display2 .= '<tr id='.$c->id.'>';
						$display2 .= '<td>'.$c->name.'</td>';
						$display2 .= '<td>'.$c->id.'</td>';
						$display2 .= '<td>'.$c2->name.'</td>';
						$display2 .= '<td>'.$c2->id.'</td>';
						$weight 	= '';
						$volume 	= '';
						foreach ($service_delivery as $sd) {
							foreach ($city_delivery_price as $key => $cdp){
								if($cdp->from_city_id == $c->id && $cdp->to_city_id == $c2->id){
									$weight_price =	json_decode($cdp->weight_price);
									if(count($weight_price) > 0){
										foreach ($weight_price as $wp) {
											if($wp->service_delivery_id == $sd->id){
												$price = $wp->price;
												$weight 	.= '<td>'.$price.'</td>';
											}
										}
									}

									$volume_price =	json_decode($cdp->volume_price);
									if(count($volume_price) > 0){
										foreach ($volume_price as $vp){
											if($vp->service_delivery_id == $sd->id){
												$price3 = $vp->price;
												$volume 	.= '<td>'.$price3.'</td>';
											}
										}
									}
								}
							}
							if($weight == ''){
								$weight .= '<td></td><td></td>';
							}

							if($volume == ''){
								$volume .= '<td></td><td></td>';
							}
						}
						$display2 .= $weight.$volume;
						$display2 .= '</tr>';
						
					}
					array_push($city_arr2, $c->id.'-'. $c2->id);
					array_push($city_arr2, $c2->id.'-'. $c->id);
					array_push($city_count_arr, $c2->id);
				}
			}

			$flagStop = 0;
			$vals = array_count_values($city_count_arr);
			if(count($vals) > 0){
				
				if(isset($vals[$c->id])){
					// if count array_count_values equal to $city
					if($vals[$c->id] == count($city) && count($city) != 1){
						// $flagStop = 1;
					}
				}
			}
			if($flagStop == 0){
				$display .= $display2;
			}

		}
		
		$display .='</tbody>
					</table>
					</div>';
		// dd($display);			
		Report::setdata($display);
		Report::settitle('Delivery Price List');
		Report::setview('admin.builder.excel2');
		Report::settype('excel');
		Report::setformat('xlsx');
		Report::setcreator('paketin');
		Report::generate();
	}

	public function filter($request){
		// dd($request->from_city);
		// dd(json_encode($arr));
		if($request->from_city == ''){
			$request->from_city = 0;
			// return 'From City Required';
		}elseif($request->to_city == ''){
			$request->to_city = 0;
			// return 'To City Required';
		}

		$query 					= City::whereIn('id', $this->formatTitikKomaToArray($this->city_id_allowed->value))->orderBy('city.province_id', 'asc');
		$query2 					= City::whereIn('id', $this->formatTitikKomaToArray($this->city_id_allowed->value))->orderBy('city.province_id', 'asc');
		$city_price_delivery  	= $this->model->get();
		$service_delivery  		= Service_delivery::where('status', 'y')->get();
		$city_delivery_price 	= City_delivery_price::get();

		// in_array if find 0 value, all city
		if($request->from_city != "" && !in_array(0, $request->from_city)){
			$query->whereIn('id', $request->from_city);
		}

		if($request->to_city != "" && !in_array(0, $request->to_city)){
			$query2->whereIn('id', $request->to_city);
		}

		$display = "";
		$get_last = "";

		$city 	= $query->get();
		$city2 	= $query2->get();

		$countServiceDelivery = count($service_delivery);
		$display .='<div class="table-scrollable">
		            <table id="table-laporan" class="table table-hover table-light">
		              <thead>
		                <tr>
		                  <th class="bg-blue-madison font-white">City / District From</th>
		                  <th class="bg-blue-madison font-white" style="display:none;">Id From</th>
		                  <th class="bg-blue-madison font-white">City / District To</th>
		                  <th class="bg-blue-madison font-white" style="display:none;">Id To</th>
		                  <th class="bg-blue-madison font-white" colspan="'.$countServiceDelivery.'" style="text-align:center;">Weight</th>
		                  <th class="bg-blue-madison font-white" colspan="'.$countServiceDelivery.'" style="text-align:center;">Volume</th>
		                </tr>
		              </thead>
		              <tbody>';

		$display .= '<tr><td class="bg-blue-madison font-white"></td><td class="bg-blue-madison font-white"></td><td class="bg-blue-madison font-white" style="display:none;"></td><td class="bg-blue-madison font-white" style="display:none;"></td>';
		
		foreach ($service_delivery as $sd) {
			$display .= '<td class="bg-blue-madison font-white">'.$sd->service_delivery_name.'</td>';
		}

		foreach ($service_delivery as $sd) {
			$display .= '<td class="bg-blue-madison font-white">'.$sd->service_delivery_name.'</td>';
		}
		$display .= '</tr>';

		
		$city_arr 		= [];
		$city_arr2 		= [];
		$city_count_arr = [];
		foreach($city as $c){
			$display2		= '';

			array_push($city_arr, $c->id);
			array_push($city_count_arr, $c->id);
			// flag if 1 = new row
			$flagrow = 0;
			foreach($city2 as $c2){
				if(!in_array($c2->id, $city_arr) && !in_array($c2->id.'-'. $c->id, $city_arr2)){

					if($flagrow == 0){
						$display2 .= '<tr id='.$c->id.'>';
						$display2 .= '<td>'.$c->name.'</td>';
						$display2 .= '<td style="display:none;">'.$c->id.'</td>';
						$display2 .= '<td>'.$c2->name.'</td>';
						$display2 .= '<td style="display:none;">'.$c2->id.'</td>';
						$weight 	= '';
						$volume 	= '';
						foreach ($service_delivery as $sd) {
							foreach ($city_delivery_price as $key => $cdp){
								if($cdp->from_city_id == $c->id && $cdp->to_city_id == $c2->id){
									$weight_price =	json_decode($cdp->weight_price);
									if(count($weight_price) > 0){
										foreach ($weight_price as $wp) {
											if($wp->service_delivery_id == $sd->id){
												$price 		= $wp->price;
												$weight 	.= '<td>'.$price.'</td>';
											}
										}
									}
									
									$volume_price =	json_decode($cdp->volume_price);
									if(count($volume_price) > 0){
										foreach ($volume_price as $vp){
											if($vp->service_delivery_id == $sd->id){
												$price3 	= $vp->price;
												$volume 	.= '<td>'.$price3.'</td>';
											}
										}
									}
								}
							}
							if($weight == ''){
								$weight .= '<td></td><td></td>';
							}

							if($volume == ''){
								$volume .= '<td></td><td></td>';
							}
						}
						$display2 .= $weight.$volume;
						$display2 .= '</tr>';
						$flagrow = 1;
					}else{
						$display2 .= '<tr id='.$c->id.'>';
						$display2 .= '<td>'.$c->name.'</td>';
						$display2 .= '<td style="display:none;">'.$c->id.'</td>';
						$display2 .= '<td>'.$c2->name.'</td>';
						$display2 .= '<td style="display:none;">'.$c2->id.'</td>';
						$weight 	= '';
						$volume 	= '';
						foreach ($service_delivery as $sd) {	
							foreach ($city_delivery_price as $key => $cdp){
								if($cdp->from_city_id == $c->id && $cdp->to_city_id == $c2->id){
									$weight_price =	json_decode($cdp->weight_price);
									if(count($weight_price) > 0){
										foreach ($weight_price as $wp) {
											if($wp->service_delivery_id == $sd->id){
												$price = $wp->price;
												$weight 	.= '<td>'.$price.'</td>';
											}
										}
									}

									$volume_price =	json_decode($cdp->volume_price);
									if(count($volume_price) > 0){
										foreach ($volume_price as $vp){
											if($vp->service_delivery_id == $sd->id){
												$price3 = $vp->price;
												$volume 	.= '<td>'.$price3.'</td>';
											}
										}
									}
								}
							}

							if($weight == ''){
								$weight .= '<td></td><td></td>';
							}

							if($volume == ''){
								$volume .= '<td></td><td></td>';
							}
						}
						$display2 .= $weight.$volume;
						$display2 .= '</tr>';
						
					}
					array_push($city_arr2, $c->id.'-'. $c2->id);
					array_push($city_arr2, $c2->id.'-'. $c->id);
					array_push($city_count_arr, $c2->id);
				}
			}

			$flagStop = 0;
			$vals = array_count_values($city_count_arr);
			if(count($vals) > 0){
				
				if(isset($vals[$c->id])){
					// if count array_count_values equal to $city
					if($vals[$c->id] == count($city) && count($city) != 1){
						// $flagStop = 1;
					}
				}
			}
			if($flagStop == 0){
				$display .= $display2;
			}

		}
		
		$display .='</tbody>
					</table>
					</div>';
		return $display;
	}

	function getCityPriceDelivery($id, $id2){
		$city_delivery_price 	= City_delivery_price::get();
		$price = "";
		foreach ($city_delivery_price as $key => $cdp){
			if($cdp->from_city_id == $id && $cdp->to_city_id == $id2){
				$weight_price =	json_decode($cdp->weight_price);
				if(count($weight_price) > 0){
					foreach ($weight_price as $wp) {
						if($wp->service_delivery_id == $sd->id){
							$price = $wp->price;
							return $price;
						}
					}
				}
			}
		}

		return $price;
	}

	public function postFiles(request $request){
		// if($request->action == 'import'){
		// dd('test');
			// return 'postFiles';

			if(!$request->hasFile('fileToUpload')){
				Alert::fail('Please upload excel file');
				return redirect()->back();
			}

			$excel 					= $request->file('fileToUpload');
			$status 				= true;
			$error_row  			= [];

			Excel::load($excel, function($reader) use(&$status,&$error_row) {
				if($reader->get()->count() == 0){
					$status = false;
					return;
				}
				$all 					= $reader->get()->toArray();
				$firstrow 				= $reader->first()->toArray();
				$service_delivery  		= Service_delivery::where('status', 'y')->get();

				// $head 		= ['username', 'password', 'email', 'user_group', 'timetable_creation_template', 'use_last_clock_in_photo', 
				// 				'location', 'activate_account', 'barcode_verified', 'force_to_log_out', 'out_of_location_alert', 'allow_overtime', 'work_on_holiday', 'first_name', 'last_name', 'mobile_phone'];

				// foreach($firstrow as $fr => $fq){
				// 	if(!in_array($fr,$head)){
				// 		$status 	= false;
				// 	}
				// }



				// if($status == true){
					$row_data 		= 0;
					$temp_username 	= [];
					$temp_arr 		= [];
					$temp_email 	= [];
					$weight_price 	= [];
					$volume_price 	= [];
					$error_row 		= [];
					// dd($all);
					foreach($all as $a){
						$row_data++;
						$flag_required = 0;
						if(empty($a['from_city_id']) || empty($a['to_city_id']) || empty($a['city_from']) || empty($a['city_to'])){
							dd($a);
							$error_row[$row_data]['required'] = 'true';
							$flag_required = 1;
						}

						$city_from_exist 	= City::where('id', $a['from_city_id'])->get();
						$city_to_exist 		= City::where('id', $a['to_city_id'])->get();
						if(count($city_from_exist) == 0 ||  count($city_to_exist) == 0){
							$city_from_exist2 	= count($city_from_exist) == 0 ? $a['from_city_id'].';' : '';
							$city_to_exist2 	= count($city_to_exist) == 0 ? $a['from_city_to'].';' : '';
							$error_row[$row_data]['citynotfound'] = 'true';
							$error_row[$row_data]['citynotfound_value'] = $city_from_exist2.$city_to_exist2;
						}

						if($flag_required == 0){
							foreach ($service_delivery as $sd) {
								if(!empty($a['weight_'.$sd['id'].'_'.strtolower(str_replace(" ", "_", $sd->service_delivery_name))])){
									$temp1 = [
										"service_delivery_id" 	=> $sd->id,
										"service_delivery_name"	=> $sd->service_delivery_name,
										"price"					=> $a['weight_'.$sd->id.'_'.strtolower(str_replace(" ", "_", $sd->service_delivery_name))]
									];

									array_push($weight_price, $temp1);
								}else if(empty($a['weight_'.$sd->id.'_'.strtolower(str_replace(" ", "_", $sd->service_delivery_name))]) && $flag_required == 0){
									$error_row[$row_data]['required'] = 'true';
									$flag_required = 1;
								}

								if(!empty($a['volume_'.$sd['id'].'_'.strtolower(str_replace(" ", "_", $sd->service_delivery_name))])){
									$temp1 = [
										"service_delivery_id" 	=> $sd->id,
										"service_delivery_name"	=> $sd->service_delivery_name,
										"price"					=> $a['volume_'.$sd->id.'_'.strtolower(str_replace(" ", "_", $sd->service_delivery_name))]
									];

									array_push($volume_price, $temp1);
								}else if(empty($a['volume_'.$sd->id.'_'.strtolower(str_replace(" ", "_", $sd->service_delivery_name))]) && $flag_required == 0){
									$error_row[$row_data]['required'] = 'true';
									$flag_required = 1;
								}
							}
							
							if(count($temp_arr) > 0){
								$flagExistData = 0;
								foreach($temp_arr as $key => $ta){
									if($ta['from_city_id'] == $a['from_city_id'] && $ta['to_city_id'] == $a['to_city_id']){
										$flagExistData 			= 1;
										// dd($a);
										$temp_arr[$key]['from_city_id'] 	= $a['from_city_id'];
										$temp_arr[$key]['to_city_id'] 		= $a['to_city_id'];
										$temp_arr[$key]['from_city_name'] 	= $a['city_from'];
										$temp_arr[$key]['to_city_name'] 	= $a['city_to'];
										$temp_arr[$key]['weight_price'] 	= json_encode($weight_price);
										$temp_arr[$key]['volume_price'] 	= json_encode($volume_price);
										// dd($temp_arr);
									}
								}

								if($flagExistData == 0){
									$temp_arr[] =[
										'from_city_id' 		=> $a['from_city_id'],
										'to_city_id' 		=> $a['to_city_id'],
										'from_city_name' 	=> $a['city_from'],
										'to_city_name' 		=> $a['city_to'],
										'weight_price' 		=> json_encode($weight_price),
										'volume_price' 		=> json_encode($volume_price),
									]; 
								}
							}else{
								$temp_arr[] =[
									'from_city_id' 		=> $a['from_city_id'],
									'to_city_id' 		=> $a['to_city_id'],
									'from_city_name' 	=> $a['city_from'],
									'to_city_name' 		=> $a['city_to'],
									'weight_price' 		=> json_encode($weight_price),
									'volume_price' 		=> json_encode($volume_price),
								]; 
							}
						}
						
						$weight_price 	= [];
						$volume_price 	= [];
					}
					
					// dd($error_row);
					if(count($error_row) < 1 && count($temp_arr) > 0){
						// dd($temp_arr);
						City_delivery_price::where('id', '!=' ,null)->delete();
						City_delivery_price::insert($temp_arr);
					}
				// }
			});
			if($status == false){
				Alert::fail(trans('general.file_wrong'));
			}else if(count($error_row) > 0){
				$request->session()->flash('error_row', $error_row);
				Alert::fail(trans('general.file_unvalid'));
			}else{
				Alert::success(trans('general.file_has_updated'));
			}
			return redirect()->back();
		// }
	}
}
