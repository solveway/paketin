<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Config;
use digipos\models\Product;
use digipos\models\Service_product;
use digipos\models\Category_service;
use digipos\models\Service_product_dt;

use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use digipos\Libraries\Email;
use Carbon\Carbon;
use File;

class ServiceProductController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "Service Product";
		$this->data['title']	= $this->title;
		$this->root_link 		= "service";
		$this->model 			= new Service_product;

		$this->bulk_action			= false;
		// $this->bulk_action_data 	= [3];
		$this->image_path 			= 'components/both/images/service_product/';
		$this->data['image_path'] 	= $this->image_path;	
		$this->image_path2 			= 'components/both/images/web/';
		$this->data['image_path2'] 	= $this->image_path2;
		$this->unit 				= ['Pcs','Psi'];

		$this->meta_title = Config::where('name', 'web_title')->first();
        $this->meta_description = Config::where('name', 'web_description')->first();
        $this->meta_keyword = Config::where('name', 'web_keywords')->first();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		$this->field = [
			[
				'name' 		=> 'service_name',
				'label' 	=> 'Service Name',
				'search' 	=> 'text',
				'sorting' 	=> 'y'
			],

			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'type' 		=> 'check',
				'data' 		=> ['y' => 'Active','n' => 'Not Active'],
				'tab' 		=> 'general'
			]
		];

		return $this->build('index');
	}

	public function create(){
		$this->data['title'] 				= "Create Service";
		$this->data['product']  			= Product::where('status', 'y')->get();
		$this->data['service_category']		= Category_service::get();
		// dd($this->data['service_category']);
		return $this->render_view('pages.service_product.create');
	}

	public function store(Request $request){
		$this->validate($request,[
			'service_name' 			=> 'required|unique:service_product',
		]);

		if($request->price == 'NaN'){
			Alert::fail('Price must be number');
			return redirect()->to($this->data['path'].'/create')->withInput($request->input());
		}

		$this->model->service_name			= $request->service_name;
		$this->model->service_category		= $request->service_category;
		$this->model->price					= $this->decode_rupiah($request->price);
		$this->model->status 				= 'y';
		$this->model->upd_by 				= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		$product_id							= $request->product_id;
		$temp_product						= [];
		if($product_id){
			$qty_default						= $request->qty_default;
			foreach ($product_id as $key => $p_id) {
				$qtyDefault		= $qty_default[$key];

				$temp_product[]			= [
					'service_product_id'	=> $this->model->id,
					'product_id'			=> $p_id,
					'qty_default'			=> $qtyDefault,
					'upd_by'				=> auth()->guard($this->guard)->user()->id,
					'created_at'			=> Carbon::now(),
					'updated_at'			=> Carbon::now()
				];
			}

			if(count($temp_product) > 0){
				// dd($temp_product);
				Service_product_dt::insert($temp_product);
			}
		}

		Alert::success('Successfully add new Service');
		return redirect()->to($this->data['path']);
	}

	public function edit($id){
		$this->model 						= $this->model->find($id);
		$this->data['title'] 				= "Edit Service ".$this->model->product_name;
		$this->data['product']  			= Product::where('status', 'y')->get();
		$this->data['service_category']		= Category_service::get();
		$this->data['data']  				= $this->model;
		$this->data['data2']  				= Service_product_dt::join('product','product.id','service_product_dt.product_id')->select('service_product_dt.*', 'product.product_name as product_name')->where('service_product_id', $id)->get();

		return $this->render_view('pages.service_product.edit');
	}

	public function update(Request $request, $id){
		$this->validate($request,[
			'service_name' 			=> 'required|unique:service_product,service_name,'.$id,
		]);

		if($request->price == 'NaN'){
			Alert::fail('Price must be number');
			return redirect()->to($this->data['path'].'/'.$id.'/edit')->withInput($request->input());
		}

		$this->model 						= $this->model->find($id);
		$this->model->service_name			= $request->service_name;
		$this->model->service_category		= $request->service_category;
		$this->model->price					= $this->decode_rupiah($request->price);
		$this->model->status 				= 'y';
		$this->model->upd_by 				= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		$product_id							= $request->product_id;
		$temp_product						= [];
		$service_product 					= Service_product_dt::where('service_product_id', $id);
		if(count($service_product->get()) > 0){
			$service_product->delete();
		}

		if($product_id){
			$qty_default						= $request->qty_default;
			foreach ($product_id as $key => $p_id) {
				$qtyDefault		= $qty_default[$key];

				$temp_product[]			= [
					'service_product_id'	=> $this->model->id,
					'product_id'			=> $p_id,
					'qty_default'			=> $qtyDefault,
					'upd_by'				=> auth()->guard($this->guard)->user()->id,
					'created_at'			=> Carbon::now(),
					'updated_at'			=> Carbon::now()
				];
			}

			if(count($temp_product) > 0){
				// dd($temp_product);
				Service_product_dt::insert($temp_product);
			}
		}
		
		$this->increase_version();
		Alert::success('Successfully add new Outlet');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->model 						= $this->model->find($id);
		$this->data['title'] 				= "View Service ".$this->model->product_name;
		$this->data['product']  			= Product::where('status', 'y')->get();
		$this->data['service_category']		= Category_service::get();
		$this->data['data']  				= $this->model;
		$this->data['data2']  				= Service_product_dt::join('product','product.id','service_product_dt.product_id')->select('service_product_dt.*', 'product.product_name as product_name')->where('service_product_id', $id)->get();

		return $this->render_view('pages.product.view');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		// dd('bulkupda');
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function export(){
		return $this->build_export_cus();
	}
}
