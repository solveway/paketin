<?php namespace digipos\Http\Controllers\Admin;

use DB;
use Session;
use Hash;
use File;

use digipos\models\User;
use digipos\models\Station;
use digipos\models\Msmenu;
use digipos\models\Useraccess;
use digipos\models\Mslanguage;
use digipos\models\City;

use digipos\Libraries\Alert;
use Illuminate\Http\Request;

class StationController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "Station";
		$this->root_url			= "administration/station";
		$this->primary_field 	= "name";
		$this->root_link 		= "station";
		$this->model 			= new Station;
		// $this->restrict_id 		= [1];
		$this->bulk_action 		= true;
		$this->bulk_action_data = [3];
		$this->image_path 		= 'components/both/images/station/';
		$this->image_path2 		= 'components/both/images/web/';
		$this->data['image_path'] 	= $this->image_path;
		$this->data['image_path2'] 	= $this->image_path2;
		// $this->merchant_id		= '';

		$this->data['root_url']		= $this->root_url;
		// $this->data['title']	= $this->title;

		// $this->data['authmenux'] = Session('authmenux'); 
		// $this->data['msmenu'] = Session('msmenu');
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'images',
				'label' => 'Image',
				'type' => 'image',
				'file_opt' => ['path' => $this->image_path]
			],
			[
				'name' => 'station_name',
				'label' => 'Station Name',
				'sorting' => 'y',
				'search' => 'text'
			],
			[
				'name' => 'phone',
				'label' => 'Phone',
				'sorting' => 'y',
				'search' => 'select',
			],
			[
				'name' => 'status',
				'label' => 'Status',
				'type' => 'check',
				'data' => ['y' => 'Active','n' => 'Not Active'],
				'tab' => 'general'
			]
		];
		return $this->build('index');

		// global
		// $this->data['user'] = $this->get_user();
		// return $this->render_view('pages.user.index');
	}

	public function create(){
		$this->data['title'] 	= 'Create New '.$this->title;
		$city_exist 			= $this->model->where('city_id', '!=', null)->get();
		
		$arr = [];
		if($city_exist){
			foreach ($city_exist as $key => $value) {
				array_push($arr, $value->city_id);
			}
		}

		$this->data['city'] 	= City::whereNotIn('id', $arr)->get();

		return $this->render_view('pages.station.create');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request){
		$this->validate($request,[
			'station_name'			=> 'required|unique:station',
			'address'				=> 'required',
			'picture' 				=> 'mimes:jpeg,png,jpg,gif'
		]);

		$this->model->station_name 		= $request->station_name;
		$this->model->address 			= $request->address;
		$this->model->phone 			= $request->phone;
		$this->model->city_id 			= $request->city;
		$this->model->status 			= 'y';

		if ($request->hasFile('images')){
        	// File::delete($this->image_path.$this->model->picture);
			$data = [
						'name' => 'images',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}
		
		$this->model->upd_by			= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully create user');
		return redirect()->to($this->data['path']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->data['data'] 		= $this->model->join('city', 'city.id', 'station.city_id')->select('station.*', 'city.name as city_name')->find($id);
		$this->data['title'] 		= 'View '.$this->title.' '.$this->data['data']->station_name;

		return $this->render_view('pages.station.view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->data['data'] 		= $this->model->find($id);
		$city_exist 				= $this->model->where('city_id', '!=', null)->get();
		// dd($city_exist);
		$arr = [];
		if($city_exist){
			foreach ($city_exist as $key => $value) {
				array_push($arr, $value->city_id);
			}
		}
		// dd($arr);
		// $this->data['city'] 		= City::whereNotIn('id', $arr)->get();
		$this->data['city'] 		= City::get();
		// dd($this->data['city'] );
		$this->data['title'] 		= 'Edit '.$this->title.' '.$this->data['data']->station_name;
		
		return $this->render_view('pages.station.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id){
		$this->validate($request,[
			'station_name'			=> 'required|unique:station,station_name,'.$id,
			'address'				=> 'required',
			'picture' 				=> 'mimes:jpeg,png,jpg,gif'
		]);
		
		$this->model 					= $this->model->find($id);
		$this->model->station_name 		= $request->station_name;
		$this->model->address 			= $request->address;
		$this->model->phone 			= $request->phone;
		$this->model->city_id 			= $request->city;
		$this->model->status 			= 'y';

		if($request->input('remove-single-image-images') == 'y'){
			if($this->model->images != null){
				File::delete($this->image_path.$this->model->images);
				$this->model->images = '';
			}
		}

		if ($request->hasFile('images')){
			$data = [
						'name' => 'images',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}
		
		$this->model->upd_by			= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully update user');
		return redirect()->to($this->data['path']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request){
		$id 		= $request->id;
		$uc 		= $this->model->find($id);
		$store 		= Station::where('id', $uc->id);
		if($uc->images != null){
			File::delete($this->image_path.$uc->images);
			$uc->images = '';
		}

		$uc->delete();
		
		Alert::success($this->title.' '.$uc->station_name.' has been deleted');
		return redirect()->back();
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function get_user(){
		return 1;
		$q = User::where('id', '!=',null)->get();
		return $q;
	}

	public function get_language(){
		$q = Mslanguage::where('status','y')->orderBy('order','asc')->pluck('language_name','id')->toArray();
		return $q;
	}

	public function export(){
		if(in_array(auth()->guard($this->guard)->user()->store_id,["0","1"])){
			$users = '';
		}else{
			$users = $this->get_userId_byStore();
		}
		return $this->build_export($users);
	}
}
