<?php namespace digipos\Http\Controllers\Admin;

use DB;
use Session;
use Hash;
use File;

use digipos\models\User;
use digipos\models\Station;
use digipos\models\Msmenu;
use digipos\models\Useraccess;
use digipos\models\Mslanguage;
use digipos\models\City;

use digipos\Libraries\Alert;
use Illuminate\Http\Request;

class UserController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "User";
		$this->root_url			= "administration/user";
		$this->primary_field 	= "id";
		$this->root_link 		= "user";
		$this->model 			= new User;
		$this->restrict_id 		= [1];
		$this->bulk_action 		= true;
		$this->bulk_action_data = [3];
		$this->image_path 		= 'components/both/images/user/';
		$this->data['image_path'] 	= $this->image_path;

		$this->data['root_url']		= $this->root_url;
		// $this->data['title']	= $this->title;

		// $this->data['authmenux'] = Session('authmenux'); 
		// $this->data['msmenu'] = Session('msmenu');
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'username',
				'label' => 'Username',
				'sorting' => 'y',
				'search' => 'text'
			],
			[
				'name' => 'name',
				'label' => 'Name',
				'sorting' => 'y',
				'search' => 'text'
			],
			[
				'name' => 'user_access_id',
				'label' => 'User Access',
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => $this->get_user_access(),
				'belongto' => ['method' => 'useraccess','field' => 'access_name']
			],
			[
				'name' => 'status',
				'label' => 'Status',
				'type' => 'check',
				'data' => ['y' => 'Active','n' => 'Not Active'],
				'tab' => 'general'
			]
		];
		return $this->build('index');

		// global
		// $this->data['user'] = $this->get_user();
		// return $this->render_view('pages.user.index');
	}

	public function create(){
		$this->data['title'] 		= 'Create New '.$this->title;
		$this->data['user_access'] 	= $this->get_user_access();
		$this->data['station'] 		= Station::where('status','y')->get();
		$this->data['city'] 		= City::join('station', 'station.city_id', 'city.id')->select('city.*')->get();
		// dd(Msmenu::get());
		return $this->render_view('pages.user.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request){
		$this->validate($request,[
			'username'				=> 'required|unique:user',
			'email'					=> 'unique:user',
			'password'				=> 'required',
			'name'					=> 'required',
			'picture' 				=> 'mimes:jpeg,png,jpg,gif'
		]);

		// select2 multiple
		// $station_value = "";
		// foreach($request->station as $key => $o){
		// 	if($key == 0) $station_value .= ";";
		// 	$station_value .= $o.";";
		// }

		$station_value = Station::where('city_id', $request->city)->first();

		$this->model->username 			= $request->username;
		$this->model->password 			= Hash::make($request->password);
		$this->model->email 			= $request->email;
		$this->model->name 				= $request->name;

		$this->model->station_id 		= $station_value->id;
		$this->model->user_access_id 	= $request->user_access_id;
		$this->model->status 			= 'y';
		$this->model->birth_date 		= date_format(date_create($request->birth_date),'Y-m-d');
		$this->model->description 		= $request->description;
		$this->model->phone 			= $request->phone;
		
		// ($request->login_web == 'y' ? $this->model->login_web = 'y' : $this->model->login_web = 'n');
		// ($request->login_app == 'y' ? $this->model->login_app = 'y' : $this->model->login_app = 'n');

		// dd($request->hasFile('logo'));
		if ($request->hasFile('picture')){
        	// File::delete($this->image_path.$this->model->picture);
			$data = [
						'name' => 'picture',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}

		if($request->input('remove-single-image-images') == 'y'){
			File::delete($this->image_path.$this->model->picture);
			$this->model->images = '';
		}
		
		$this->model->upd_by			= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully create user');
		return redirect()->to($this->data['path']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->data['user'] 	= $this->model->find($id);
		$this->data['user2'] 	= $this->model->join('station', 'station.id', 'user.station_id')->join('city', 'city.id', 'station.city_id')->select('station.*', 'city.name as city_name', 'station.station_name as station_name')->find($id);
		
		$this->data['title'] = 'View '.$this->title.' '.$this->data['user']->username;	
		$this->data['user_access'] = $this->get_user_access();
		// dd($this->data['user_access']);
		$this->data['ouser'] = "";
		$oulet_id = explode(";", $this->data['user']->outlet_id);
		$i = 0;
		foreach($oulet_id as $key => $o){
			if($o != ""){
				$oname = Outlet::find($o)->outlet_name;
				if($i != 0) $this->data['ouser'] .= ", ";
				$this->data['ouser'] .= $oname;

				$i++;
			}
		}

		return $this->render_view('pages.user.view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->data['user'] 		= $this->model->find($id);
		// dd($this->data['user']);
		$this->data['title'] 		= 'Edit '.$this->title.' '.$this->data['user']->username;	
		$this->data['user_access'] 	= $this->get_user_access();
		$this->data['station'] 		= Station::where('status','y')->get();
		$this->data['city'] 		= City::join('station', 'station.city_id', 'city.id')->select('city.*')->get();
		
		return $this->render_view('pages.user.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id){
		$this->validate($request,[
			'username'				=> 'required|unique:user,username,'.$id,
			'email'					=> 'unique:user,email,'.$id,
			'name'					=> 'required',
			'picture' 				=> 'mimes:jpeg,png,jpg,gif'
		]);
		
		// $station_value = "";
		// foreach($request->station as $key => $o){
		// 	if($key == 0) $station_value .= ";";
		// 	$station_value .= $o.";";
		// }

		$station_value = Station::where('city_id', $request->city)->first();

		$this->model 					= $this->model->find($id);
		$this->model->username 			= $request->username;
		$this->model->password 			= ($request->password != "" ? Hash::make($request->password) : $this->model->password);
		$this->model->email 			= $request->email;
		$this->model->name 				= $request->name;
		$this->model->station_id 		= $station_value->id;
		$this->model->user_access_id 	= $request->user_access_id;
		$this->model->phone 			= $request->phone;
		$this->model->status 			= 'y';
		$this->model->birth_date 		= date_format(date_create($request->birth_date),'Y-m-d');
		$this->model->description 		= $request->description;

		// ($request->login_web == 'y' ? $this->model->login_backend = 'y' : $this->model->login_backend = 'n');
		// ($request->login_app == 'y' ? $this->model->login_app = 'y' : $this->model->login_app = 'n');

		if($request->input('remove-single-image-picture') == 'y'){
			if($this->model->images != null){
				File::delete($this->image_path.$this->model->images);
				$this->model->images = '';
			}
		}

		if ($request->hasFile('picture')){
			$data = [
						'name' => 'picture',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}
		
		$this->model->upd_by			= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully update user');
		return redirect()->to($this->data['path']);
	}

	public function store_station(Request $request){
		
		// dd($request->s_name.' - '.$request->phone.' - '.$request->address);
		$station 					= new Station;
		$exist 						= $station->where('station_name',$request->station_name)->count();

		if($exist > 0){
			return response()->json(['status' => 'exist']); 
		}else{
			$station->station_name 		= $request->station_name;
			$station->address 			= $request->address;
			$station->phone 			= $request->phone;
			$station->status 			= 'y';
			
			$this->model->upd_by			= auth()->guard($this->guard)->user()->id;
			// dd($station);
			$station->save();

			return response()->json(['status' => 'success']); 
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(){
		$this->field = $this->field_edit();
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function get_user(){
		return 1;
		$q = User::where('id', '!=',null)->get();
		return $q;
	}

	public function get_language(){
		$q = Mslanguage::where('status','y')->orderBy('order','asc')->pluck('language_name','id')->toArray();
		return $q;
	}

	public function export(){
		if(in_array(auth()->guard($this->guard)->user()->store_id,["0","1"])){
			$users = '';
		}else{
			$users = $this->get_userId_byStore();
		}
		return $this->build_export($users);
	}
}
