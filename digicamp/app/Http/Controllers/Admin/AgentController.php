<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Config;
use digipos\models\Agent;
use digipos\models\Service_delivery;
use digipos\models\City;

use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use digipos\Libraries\Email;
use Carbon\Carbon;
use File;

class AgentController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 				= "Agent";
		$this->data['title']		= $this->title;
		$this->root_link 			= "agent";
		$this->model 				= new Agent;

		$this->bulk_action			= true;
		$this->bulk_action_data 	= [3];
		$this->image_path 			= 'components/both/images/agent/';
		$this->data['image_path'] 	= $this->image_path;
		$this->image_path2 			= 'components/both/images/web/';
		$this->data['image_path2'] 	= $this->image_path2;

		$this->meta_title = Config::where('name', 'web_title')->first();
        $this->meta_description = Config::where('name', 'web_description')->first();
        $this->meta_keyword = Config::where('name', 'web_keywords')->first();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		// $desc_filter = Order_status::select('desc')->whereIn('id', [1,2,3,4,5,6,11])->get();

		// foreach($desc_filter as $dc){
		// 	$dc_filter[$dc->desc] = $dc->desc;
		// }

		$this->field = [
			[
				'name' => 'business_name',
				'label' => 'Business Name',
				'type' => 'text',
				'file_opt' => ['path' => $this->image_path]
			],
			[
				'name' 		=> 'business_phone',
				'label' 	=> 'Business Phone',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'business_email',
				'label' 	=> 'Business Email',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' 	=> 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];

		// $this->model = $this->model->join('order_status', 'order_status.id', 'orderhd.order_status')->where('type_order', 'not like', '%post%')->select('orderhd.*', 'order_status.desc');
		return $this->build('index');
	}

	public function create(){
		
		$this->data['title'] 			= "Create Agent";
		$this->data['city']  			= City::get();
		// $this->data['province']		= Province::get();

		return $this->render_view('pages.agent.create');
	}

	public function store(Request $request){
		$this->validate($request,[
			'business_email' 		=> 'required|unique:agent,business_email',
		]);

		$this->model->business_name					= $request->business_name;
		$this->model->business_address				= $request->business_address;
		$this->model->formal_business_name			= $request->formal_business_name;
		$this->model->business_cp_full_name_title	= $request->business_cp_full_name_title;
		$this->model->business_email				= $request->business_email;
		$this->model->password						=  Hash::make($request->business_email);
		$this->model->business_phone				=  $request->business_phone;
		$this->model->city_id						=  $request->city;

		$get_notified_by 	= '';
		($request->sms == 'y' ? $get_notified_by .= 'sms;' : '');
		($request->email == 'y' ? $get_notified_by .= 'email;' : '');
		if($get_notified_by != ''){
			$get_notified_by = ';'.$get_notified_by;
		}


		$this->model->get_notified_by				=  $get_notified_by;

		$this->model->billing_cp_full_name_title	=  $request->billing_cp_name_title;
		$this->model->billing_address				=  $request->billing_address;
		$this->model->billing_email					=  $request->billing_email;
		$this->model->billing_phone					=  $request->billing_phone;


		$payment_method_id = '';
		($request->monthly_clearing == 'y' ? $payment_method_id .= '1;' : '');
		($request->weekly_clearing == 'y' ? $payment_method_id .= '2;' : '');
		if($payment_method_id != ''){
			$payment_method_id = ';'.$payment_method_id;
		}

		$this->model->payment_method_id				= $payment_method_id;
		$this->model->billing_address				= $request->billing_address;

		$this->model->status 						= 'y';
		$this->model->upd_by 						= auth()->guard($this->guard)->user()->id;

		// if ($request->hasFile('image')){
  //       	// File::delete($path.$user->images);
		// 	$data = [
		// 				'name' => 'image',
		// 				'file_opt' => ['path' => $this->image_path.$curr_id.'/']
		// 			];
		// 	$image = $this->build_image($data);
		// 	$this->model->images = $image;
		// }

		// dd($this->model);		
		$this->model->save();

		Alert::success('Successfully add new Outlet');
		return redirect()->to($this->data['path']);
	}

	public function edit($id){
		$this->model 					= $this->model->find($id);
		$this->data['data'] 			= $this->model;
		$this->data['title'] 			= "Edit Agent ".$this->model->business_name;
		$this->data['city']  			= City::get();
		// $this->data['province']		= Province::get();

		// dd(in_array('sms', explode(';', $this->model->get_notified_by)));

		return $this->render_view('pages.agent.edit');
	}

	public function update(Request $request, $id){
		$this->validate($request,[
			'business_email' 	=> 'required|unique:agent,business_email,'.$id,
		]);

		$this->model 								= $this->model->find($id);
		$this->model->business_name					= $request->business_name;
		$this->model->business_address				= $request->business_address;
		$this->model->formal_business_name			= $request->formal_business_name;
		$this->model->business_cp_full_name_title	= $request->business_cp_full_name_title;
		$this->model->business_email				= $request->business_email;
		$this->model->password						=  Hash::make($request->business_email);
		$this->model->business_phone				=  $request->business_phone;
		$this->model->city_id						=  $request->city;

		$get_notified_by 	= '';
		($request->sms == 'y' ? $get_notified_by .= 'sms;' : '');
		($request->email == 'y' ? $get_notified_by .= 'email;' : '');
		if($get_notified_by != ''){
			$get_notified_by = ';'.$get_notified_by;
		}


		$this->model->get_notified_by				=  $get_notified_by;

		$this->model->billing_cp_full_name_title	=  $request->billing_cp_name_title;
		$this->model->billing_address				=  $request->billing_address;
		$this->model->billing_email					=  $request->billing_email;
		$this->model->billing_phone					=  $request->billing_phone;


		$payment_method_id = '';
		($request->monthly_clearing == 'y' ? $payment_method_id .= '1;' : '');
		($request->weekly_clearing == 'y' ? $payment_method_id .= '2;' : '');
		if($payment_method_id != ''){
			$payment_method_id = ';'.$payment_method_id;
		}

		$this->model->payment_method_id				=  $payment_method_id;
		$this->model->billing_address				= $request->billing_address;


		// if($request->input('remove-single-image-image') == 'y'){
		// 	if($this->model->images != NULL){
		// 		File::delete($this->image_path.$this->model->id.'/'.$this->model->images);
		// 		$this->model->images = '';
		// 	}
		// }

		// if ($request->hasFile('image')){
  //       	// File::delete($path.$user->images);
		// 	$data = [
		// 				'name' => 'image',
		// 				'file_opt' => ['path' => $this->image_path.$this->model->id.'/']
		// 			];
		// 	$image = $this->build_image($data);
		// 	$this->model->images = $image;
		// }

		// dd($this->model);
		$this->model->save();
		
		Alert::success('Successfully add new Outlet');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->model 					= $this->model->find($id);
		$this->data['title'] 			= "View Product ".$this->model->product_name;
		// $this->data['unit']  			= $this->unit;
		$this->data['data']  			= $this->model;
		return $this->render_view('pages.agent.view');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		// dd('bulkupda');
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function export(){
		return $this->build_export_cus();
	}
}
