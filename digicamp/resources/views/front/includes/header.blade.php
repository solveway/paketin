@if($path[0] == 'forgot_password' || $path[0] == 'forget-password')
	<div class="page-header navbar navbar-fixed-top">
	  <div class="page-header-inner ">
	    <div class="page-logo">
	        <a href="{{url($root_path)}}">
	          <img src="{{asset('components/back/images/admin')}}/{{$web_logo}}" alt="logo" class="logo-default"/>
	        </a>
	        <div class="menu-toggler sidebar-toggler"></div>
	    </div>
	    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
	    <div class="page-top">
	      <div class="top-menu">

	      	
	      		<ul class="nav navbar-nav pull-right">
		          <li class="dropdown dropdown-user dropdown-dark">
		            <a href="{{url('/')}}" class="dropdown-toggle"><span class="username"> Sign In </span></a>
		          </li>
		        </ul>
	      </div>
	    </div>
	  </div>
	</div>
@else
        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <img src="{{asset('components/back/images/admin')}}/{{$web_logo}}" alt="logo" class="logo-default"/>
                </div>

                <ul class="list-unstyled components">
                    <p>Dummy Heading</p>
                    <li class="active">
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">Home</a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <li><a href="#">Home 1</a></li>
                            <li><a href="#">Home 2</a></li>
                            <li><a href="#">Home 3</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">About</a>
                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">Pages</a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li><a href="#">Page 1</a></li>
                            <li><a href="#">Page 2</a></li>
                            <li><a href="#">Page 3</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Portfolio</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>

                <ul class="list-unstyled CTAs">
                    <li><a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a></li>
                    <li><a href="https://bootstrapious.com/p/bootstrap-sidebar" class="article">Back to article</a></li>
                </ul>
            </nav>
@endif