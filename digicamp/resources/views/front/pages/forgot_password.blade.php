@extends($view_path.'.layouts.master')
@section('content')
<style>
  
</style>
<div class="page-container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2 page-forgot-password">
			<div class="panel-heading-style1">Forgot Password ?</div>
			<div class="panel-heading-style2">To reset your account password. Enter the email address you registered with and we'll send you instructions.</div>
			<div class="panel-body">
				@if(session()->has('err'))
                    <div class="growl-alert" data-type="danger" data-message="{{ session()->get('err') }}">
                    </div>
                @endif

                @if(session()->has('suc'))
                    <div class="growl-alert" data-type="info" data-message="{{ session()->get('suc') }}">
                    </div>
                @endif

				<form class="form-horizontal" method="post" action="{{url('forgot_password/send_reset_password')}}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="form-group">
						<label class="col-md-4 control-label">E-Mail Address</label>
						<div class="col-md-6">
							<input type="email" class="form-control" name="email" value="{{ old('email') }}">
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-primary">
								Send Password Reset Link
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@push('custom_scripts')
<script>

</script>
@endpush
