@extends($view_path.'.layouts.master')
@section('content')
<div class="row fp_con page-container">
  <div class="row cus_con">
  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  	  <div class="row">
  	  	<div class="col-lg-offset-3 col-lg-6 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-12 fp_cons">
  	  	  <div class="row">
     		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fp_text page-forgot-password">
     		  <div class="panel-heading-style1">New Password</div>
     		</div>

     		@if (count($errors) > 0)
			<div class="col-md-12 col-sm-12 col-xs-12">
			    <div class="alert alert-danger alert-dismissable">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			</div>
			@endif
			
			@if($status == "found")
			<div class="col-md-12 col-sm-12 col-xs-12 panel-body">
				<form class="form-horizontal" role="form" method="post" action="confirm-password">
				{{csrf_field()}}

				<input type="hidden" name="sc_tk" value="{{ $token }}" />

				<div class="form-group">
					<label class="col-md-4 col-sm-5 control-label fp_input">Password :</label>
					<div class="col-md-6 col-sm-6 fp_input">
						<input type="password" class="form-control" name="password">
					</div>
				</div>

				<div class="form-group fp_input">
					<label class="col-md-4 col-sm-5 control-label fp_input">Confirm Password :</label>
					<div class="col-md-6 col-sm-6 fp_input">
						<input type="password" class="form-control" name="password_confirmation">
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6 col-md-offset-4 col-sm-6 col-sm-offset-5">
						<button type="submit" class="btn fp_submit btn-primary">
							Submit
						</button>
					</div>
				</div>
				</form>
			</div>
			@else
			<div class="col-md-12 col-sm-12 col-xs-12">	
				<h1>Sorry Your Request Goes Wrong Or Token Already Been Used, Please Forgot Password again or Contact Our Team in {{ $web_email }}</h1>
			</div>
			@endif
     	  </div>
  	  	</div>
  	  </div>
  	</div>
  </div>
</div>
@endsection