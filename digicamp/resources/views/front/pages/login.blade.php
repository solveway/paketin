@extends($view_path.'.layouts.master_login')
@section('content')
<div class="login-page col-md-offset-2 col-md-8">
    <div class="information-blocks">
        <div class="row">
            <div class="col-sm-5 information-entry">
                <div class="login-box login-box-left">
                    <div class="login-logo">
                        <img src="{{asset('components/back/images/admin')}}/{{$web_logo}}" width="50%"></img>
                    </div>
                    <div class="login-desc">
                        <div class="padding-10px"><span class="login-desc-title">Kirim</span><br>
                        Jadwal pesanan baru dengan mudah</div>
                        <div class="padding-10px"><span class="login-desc-title">Jalur</span><br>
                        Ketahuilah kemana paket anda setiap saat.
                        Update status pengiriman real time memastikan pengiriman aman ke tujuan.</div>
                        <div class="padding-10px"><span class="login-desc-title">Mengelola</span><br>
                        Mengelola beberapa pesanan sekaligus dengan mulus melalui satu platform.</div>
                   
                        <div class="padding-10px">
                            <a class="login-desc-title login-footer" href="{{url('http://paketin.id/')}}">www.paketin.id</a>
                        </div>
                        <div class="padding-10px">
                            <a class="login-desc-copyright login-footer" href="{{url('http://solveway.co.id/')}}">&copy; Copyright Solveway {{date('Y')}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-7 information-entry">
                <div class="login-box login-box-right" id="background_blue">
                    <div class="article-container style-1">
                        <h3 class="login-title">Login</h3>
                        <form action="{{ url('lock_in') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            @if(session()->has('err'))
                                <div class="growl-alert" data-type="danger" data-message="{{ session()->get('err') }}">
                                </div>
                            @endif

                            @if(session()->has('suc'))
                                <div class="growl-alert" data-type="info" data-message="{{ session()->get('suc') }}">
                                </div>
                            @endif
                            <div class="col-md-12 float-left-none">
                                <label>USER NAME</label>
                                <div class="form-group form-md-line-input">
                                    <input type="text" id="" class="form-control" name="lock_em" value="" required="" autofocus="" placeholder="User Name">
                                    <small></small>
                                </div>
                           
                                <label>PASSWORD</label>
                                <div class="form-group form-md-line-input">
                                    <input type="password" id="" class="form-control" name="lock_pw" value="" required="" autofocus="" placeholder="Password">
                                    <small></small>
                                </div>

                                <div class="btn-log-in">
                                    <button type="submit" class="button style-12 btn-login form-control">LOG IN</button>
                                </div>
                              
                            </div>
                        </form>
                        <div class="forgot-password">
                            <form action="{{ url('forgot_password') }}" method="get" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                <button type="submit" class="btn hd_forgot">Forgot Password ?</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
 <div class="modal fade in" id="lockforgot" role="dialog" style="display: none; padding-right: 10px;">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header fg_1">
            <span>Forgot Your Password</span>
            <button type="button" class="close" id="lock_close" data-dismiss="modal">×</button>
          </div>

          <div class="modal-body fg_2">
            <div class="row">
              <form method="post" action="{{url('forgot')}}">
                <input type="hidden" name="_token" value="Oo11D9m7VGFWcJnR383etjvybxFWw1bTMMWgJw4N">
                <div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8"> 
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="form-group">
                        <input class="form-control" type="email" placeholder="Email Address" name="email" value="" required="">
                      </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <button type="submit" class="fg_3 btn">Submit</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        
    });
</script>
@endpush
@endsection