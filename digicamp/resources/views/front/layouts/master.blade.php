<!DOCTYPE html>
<html lang="id">
	<head>
		@include($view_path.'.includes.head')
	</head>
	<body class="style-10">
	    <div id="loader-wrapper">
	        <div class="bubbles">
	            <div class="title">loading</div>
	            <span></span>
	            <span id="bubble2"></span>
	            <span id="bubble3"></span>
	        </div>
	    </div>
	    
		@include($view_path.'.includes.header')
		<div class="container-fluid">
			@yield('content')
		</div>
		@include($view_path.'.includes.footer')
	    
    	<div class="clear"></div>
	</body>
</html>