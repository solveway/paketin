<!DOCTYPE html>
<html lang="id">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="format-detection" content="telephone=no" />
		<meta http-equiv="Content-Language" content="id">
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
		<meta content="{{$web_keywords}}" name="keywords" />
		<meta content="{{$web_description}}" name="description" />
		<title>{{$web_name}}</title>

		<meta content="Wahana" name="keywords" />
		<meta content="Website Wahana" name="description" />
		<meta name="geo.placename" content="Indonesia">
		<meta name="geo.country" content="ID">
		<meta name="language" content="Indonesian">
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<meta name="root_url" content="{{url($root_path)}}/" />
		<link rel="icon" href="{{asset('components/back/images/admin')}}/{{$favicon}}" type="image/x-icon">
		<!-- Core CSS -->
		<link rel ="stylesheet" href="{{asset('components/plugins/bootstrap/css/bootstrap.min.css')}}">
		<link rel ="stylesheet" href="{{asset('components/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
		<link rel ="stylesheet" href="{{asset('components/front/css/idangerous.swiper.css')}}">
		<link rel ="stylesheet" href="{{asset('components/plugins/swiper-slider/swiper.min.css')}}">
		<link rel ="stylesheet" href="{{asset('components/front/css/style.css')}}">
		<link rel ="stylesheet" href="{{asset('components/front/css/custom_style.css')}}">
		<link rel ="stylesheet" href="{{asset('components/front/css/app.css')}}">
	</head>
	<body class="style-10">
	    <div id="loader-wrapper">
	        <div class="bubbles">
	            <div class="title">loading</div>
	            <span></span>
	            <span id="bubble2"></span>
	            <span id="bubble3"></span>
	        </div>
	    </div>
	    
		
		<div class="container-fluid">
			@yield('content')
		</div>
	    <div class="footer-wrapper style-10">
	    <!-- <footer class="type-1">
	        <div class="footer-columns-entry">
	            <div class="row">
	                <div class="col-md-6">
	                    <div class="col-md-6">
	                        <a href="{{url('/')}}"><img src="{{asset('components/both/images/web')}}/Relationship Solution.png" alt="{{$web_name}}" class="img-footer" /></a>
	                    </div>
	                    <div class="col-md-6">
	                        <a href="{{url('/')}}"><img src="{{asset('components/both/images/web')}}/Customer Care.png" alt="{{$web_name}}" class="img-footer" /></a>
	                    </div>

	                </div>
	                <div class="col-md-6">
	                    <div class="col-md-12">
	                        <div class="socials-box">
	                           
	                        </div>
	                    </div>
	                     <div class="copyright">  
	                        <div class="col-md-12">
	                            Copyright {{date('Y')}}, Wahana<br>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-md-12 solveway-dev">
	                    <a href="{{url('http://solveway.co.id/')}}">Designed and Developed by PT. Solveway Dashindo Teknologi</a>
	                </div>
	            </div>
	        </div>
	    </footer> -->
	</div>

	<script type="text/javascript" src="{{asset('components/plugins/jquery.min.js')}}"></script>
	<script src="{{asset('components/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('components/front/js/idangerous.swiper.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('components/plugins/swiper-slider/swiper.jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('components/front/js/global.js')}}"></script>
	<script type="text/javascript" src="{{asset('components/front/js/jquery.mousewheel.js')}}"></script>
	<script type="text/javascript" src="{{asset('components/front/js/jquery.jscrollpane.min.js')}}"></script>
	<script src="{{asset('components/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('components/front/js/kyubi-head.js')}}"></script>

	@stack('custom_scripts')
    	<div class="clear"></div>
	</body>
</html>