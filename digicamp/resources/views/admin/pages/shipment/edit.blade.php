@extends($view_path.'.layouts.master')
@section('content')

@push('styles')
<style>

</style>

<form role="form" method="post" action="{{url($path)}}/{{$data->id}}" enctype="multipart/form-data">
{{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
          @include('admin.includes.errors')
          <div class="row">
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'shipping_no','label' => 'Shipping No.','value' => (old('shipping_no') ? old('shipping_no') : $data->shipping_no),'attribute' => 'required autofocus disabled','form_class' => 'col-md-12', 'class' => ''])!!}

            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'status_name','label' => 'Status Shipment','value' => (old('status_name') ? old('status_name') : $data->status_name),'attribute' => 'required autofocus disabled','form_class' => 'col-md-12', 'class' => ''])!!}

            <div class="col-md-12">
              <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
              <h4>Sender</h4>
              </div>
            </div>

            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'sender_name','label' => 'Sender Name','value' => (old('sender_name') ? old('sender_name') : $data->sender_name),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}

            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'sender_no_ktp','label' => 'Sender No. KTP','value' => (old('sender_no_ktp') ? old('sender_no_ktp') : $data->sender_no_ktp),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}

            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'sender_address','label' => 'Sender Address','value' => (old('sender_address') ? old('sender_address') : $data->start_address),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}

            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'sender_hp','label' => 'Sender Handphone Number ','value' => (old('sender_hp') ? old('sender_hp') : $data->sender_hp),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}

            <div class="col-md-12">
              <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
              <h4>Receiver</h4>
              </div>
            </div>

            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'receipt_name','label' => 'Receiver Name','value' => (old('receipt_name') ? old('receipt_name') : $data->receipt_name),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}

            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'receipt_hp','label' => 'Receipt HP','value' => (old('receipt_hp') ? old('receipt_hp') : $data->receipt_hp),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}

            {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'dest_address','label' => 'Destination Address','value' => (old('dest_address') ? old('dest_address') : $data->dest_address),'attribute' => 'required autofocus','form_class' => 'col-md-12', 'class' => ''])!!}


            <div class="col-md-6">
                <div class="form-group form-md-line-input" style="padding-top: 0;">
                    <label>Start District (include station)</label>
                    <select id="start_station_id" name="start_district" class="form-control select2">
                      <optgroup label="Start District">
                        <option value="">-- Please Select Start District --</option>
                        @foreach($city as $o)
                          <option value="{{$o->id}}" {{$o->id == $data->start_district ? 'selected' : ''}}>{{$o->name}}</option>
                        @endforeach
                      </optgroup>
                    </select>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group form-md-line-input" style="padding-top: 0;">
                    <label>Destination District (include station)</label>
                    <select id="dest_station_id" name="dest_district" class="form-control select2">
                      <optgroup label="Destination District">
                         <option value="">-- Please Select Destination District --</option>
                        @foreach($city as $o)
                          <option value="{{$o->id}}" {{$o->id == $data->dest_district ? 'selected' : ''}}>{{$o->name}}</option>
                        @endforeach
                      </optgroup>
                    </select>
                </div>
            </div>

            @if($data->shipment_status_id == 1 || $data->shipment_status_id == 11)
              <!-- <div class="col-md-12">
                <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
                <h4>Assign Driver</h4>
                </div>
              </div>

              <div class="col-md-6">
                  <div class="form-group form-md-line-input" style="padding-top: 0;">
                      <label>Assign to Driver (include station)</label>
                      <select id="user_id" name="user_id" class="form-control select2">
                        <optgroup label="Destination District">
                           <option value="">-- Please Select Driver --</option>
                          @foreach($driver as $o)
                            <option value="{{$o->id}}">{{$o->name.' ('.$o->username.')'}}</option>
                          @endforeach
                        </optgroup>
                      </select>
                  </div>
              </div> -->
            @endif

          <div class="col-md-12">
            <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
            <h4>Destination Location</h4>
            </div>
          </div>

          <div class="col-md-12"> 
              <input id="pac-input" class="controls" type="text" placeholder="Search Box">
              <div id="map" class="gmaps_outlet"> </div>
              <input type="hidden" name="latitude" id="latbox" value="{{$data->latitude}}">
              <input type="hidden" name="longitude" id="lngbox" value="{{$data->longitude}}">
          </div>
          <div class="clearfix"></div>

          <div class="col-md-12">
            <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
            <h4>Package</h4>
            </div>
          </div>
          
          @if(isset($shipment_size_log_before))
            <div class="col-md-12">
                 <a tabindex="0" class="" role="button" data-toggle="popover" data-trigger="focus" title="Dismissible popover" data-content="And here's some amazing content. It's very engaging. Right?" id="package_before2">Dismissible popover<br></a>
            </div>
          @endif

            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'contents_of_the_package','label' => 'Contents of The Package','value' => (old('contents_of_the_package') ? old('contents_of_the_package') : $data->description),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}

            <div class="col-md-6">
                <div class="form-group form-md-line-input" style="padding-top: 0;">
                    <label>Service Delivery</label>
                    <select id="service_delivery" name="service_delivery" class="form-control select2">
                      <optgroup label="Service Delivery">
                        @foreach($service_delivery as $o)
                          <option value="{{$o->id}}" {{$o->id == $data->service_delivery_id ? 'selected' : ''}}>{{$o->service_delivery_name.' - '.$o->description}}</option>
                        @endforeach
                      </optgroup>
                    </select>
                </div>
            </div>

            <div class="col-md-6">
              <div class="form-group form-md-line-input" qoq="">
                <input type="number" id="form_floating_Qoq" class="form-control" name="number_of_deposit" value="{{$data->number_of_deposit}}" required="" autofocus="" placeholder="Qty of Goods">
                <label for="form_floating_Qoq">Number of Deposit <span class="required" aria-required="true"></span></label>
                <small></small>
              </div>
            </div>
            
            <div class="col-md-6">
              <div class="form-group form-md-line-input" qoq="">
                <input type="number" id="form_floating_Qoq" class="form-control" name="weight" value="{{isset($shipment_size_log) ? $shipment_size_log['weight'] : ''}}" required="" autofocus="" placeholder="Weight" disabled>
                <label for="form_floating_Qoq">Weight (Kg)<span class="required" aria-required="true"></span></label>
                <small></small>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group form-md-line-input" qoq="">
                <div class="col-md-4 padding-left-0">
                  <input type="number" id="form_floating_Qoq" class="form-control col-md-6" name="long" value="{{$long}}" required="" autofocus="" placeholder="Long" disabled>
                </div>
                <div class="col-md-4 padding-left-0">
                  <input type="number" id="form_floating_Qoq" class="form-control " name="wide" value="{{$wide}}" required="" autofocus="" placeholder="Wide" disabled>
                </div>
                <div class="col-md-4 padding-left-0">
                  <input type="number" id="form_floating_Qoq" class="form-control " name="height" value="{{$height}}" required="" autofocus="" placeholder="Height" disabled>
                </div>
                <label for="form_floating_Qoq" class="label-volume">Volume (cm)<span class="required" aria-required="true"></span></label>
                <small></small>
              </div>
            </div>

            <div class="col-md-12">
              <div class="form-group form-md-line-input col-md-2">
                  <div class="md-checkbox">
                    <label>Packing</label>
                  <input type="checkbox" id="checkbox_form_1" class="md-check" name="packing" value="y" {{isset($data->packing) ? ($data->packing == 'y' ? 'checked' : '') : ''}}>
                  <label for="checkbox_form_1">
                    <span></span>
                        <span class="check"></span>
                        <span class="box"></span>
                  </label>
                </div>
              </div>

              <div class="form-group form-md-line-input col-md-2">
                  <div class="md-checkbox">
                      <label>Insurance</label>
                    <input type="checkbox" id="checkbox_form_2" class="md-check" name="insurance" value="y" {{isset($data->asurance) ? ($data->asurance == 'y' ? 'checked' : '') : ''}}>
                    <label for="checkbox_form_2">
                      <span></span>
                          <span class="check"></span>
                          <span class="box"></span>
                    </label>
                  </div>
              </div>
            </div>
          </div>

          <div class="row">
            {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}

            @if($data->shipment_status_id == 1 || $data->shipment_status_id == 11)
                <a href="{{url($path)}}"><button type="button" class="btn red-mint">Cancel</button></a>
            @endif
          </div>
    </div>
  </div>
</form>
@endsection

@push('custom_scripts')
  <script>
    $(document).ready(function(){
      $('#package_before').tooltip();
      $('#package_before2').popover();
      // $('input,select,textarea,checkbox,.remove-single-image').prop('disabled',true);
      tinymce.settings = $.extend(tinymce.settings, { readonly: 1 });

      setTimeout(function(){
          var lat = parseFloat({{$data->latitude}});
          var lng = parseFloat({{$data->longitude}});

          google.maps.event.trigger(map, 'resize'); 
        
          Markerlatlng = new google.maps.LatLng(lat, lng);
          map.setCenter(Markerlatlng); // setCenter takes a LatLng object
      }, 2500);
    });

    $( ".buying_price" ).blur(function() {  
        // alert('test');
        //number-format the user input
        var val = $(this).val();
        var val2 = parseFloat(val.replace(/,/g, ""))
                      .toFixed(2)
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this).val(val2);             
    });

    $( ".adjust" ).blur(function() {
        var val = $(this).val();
        console.log(val);
        if(val < 0){
          $(this).val(0);
        }
    });

    function initAutocomplete() {
        var lat = parseFloat({{$data->latitude}});
        var lng = parseFloat({{$data->longitude}});


        markers = [];
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          styles: [ {"featureType":"all",   "stylers":[     {"saturation":0},     {"hue":"#e7ecf0"}   ] },  {"featureType":"road",    "stylers":[     {"saturation":-70}    ] },  {"featureType":"transit",   "stylers":[     {"visibility":"off"}    ] },  {"featureType":"poi",   "stylers":[     {"visibility":"off"}    ] },  {"featureType":"water",   "stylers":[     {"visibility":"simplified"},      {"saturation":-60}    ] }]
        }); 

        var marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: map,
            draggable:true,
            zoom:12
        });

        markers.push(marker);

        google.maps.event.addListener(marker, 'dragstart', function(event){
            document.getElementById("latbox").value = event.latLng.lat();
            document.getElementById("lngbox").value = event.latLng.lng();
        });

        google.maps.event.addListener(marker, 'dragend', function(event){
            document.getElementById("latbox").value = event.latLng.lat();
            document.getElementById("lngbox").value = event.latLng.lng();
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          for(var i = 0;i < 1; i++){
            place = places[i];
            var marker = new google.maps.Marker({
              map: map,
              title: place.name,
              position: place.geometry.location,
              draggable:true,
              zoom:1
            });
            markers.push(marker);
            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }

            document.getElementById("latbox").value = place.geometry.location.lat();
            document.getElementById("lngbox").value = place.geometry.location.lng();

            google.maps.event.addListener(marker, 'dragstart', function(event){
                document.getElementById("latbox").value = event.latLng.lat();
                document.getElementById("lngbox").value = event.latLng.lng();
            });
            google.maps.event.addListener(marker, 'dragend', function(event){
                document.getElementById("latbox").value = event.latLng.lat();
                document.getElementById("lngbox").value = event.latLng.lng();
            });
          }
          map.fitBounds(bounds);
        });
    }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpMppLOBQyYyKg11P20E9pe4Hs-cwlH9U&libraries=places&callback=initAutocomplete" async defer></script>
@endpush
