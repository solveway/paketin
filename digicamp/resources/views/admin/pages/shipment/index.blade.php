@extends($view_path.'.layouts.master')
@push('css')
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="{{asset('components/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('components/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('components/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('components/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('components/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->

  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="{{asset('components/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('components/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->

    <style type="text/css">
      .table-heading{
        border: 1px solid #ddd !important;
      }

       .table.table-light > tbody > tr > td, {
        border: 1px solid #ddd;
      }

      .table.table-light > tbody > tr > td {
        border: 1px solid #ddd !important;
      }

      .table>thead:first-child>tr:first-child>th {
        border: 1px solid #ddd !important;
      }
  </style>
@endpush
@section('content')
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
    </div>
    <div class="portlet-body form">
      <!--<form role="form" class="form-horizontal" method="post" action="{{url($path)}}/ext/postFiles" enctype="multipart/form-data"> -->
        @include('admin.includes.errors')
        <div class="form-body">

            <div class="table-responsive">
              <table class="table table-bordered">
                  <form method="get" id="builder_form" action="http://localhost/paketin/_admin/shipment/manage-shipment" data-ask="n"></form>
                  <input type="hidden" name="_token" value="uxS0zxNOn2zughMw8wIC1NYUG8TKiZSXkAp7xtMY">
                    <tbody>
                        <tr class="search-head-content" style="display:none;">
                          <th></th>
                          <th>
                            <div class="form-group form-md-line-input">
                            <input type="text" placeholder="Shipping No" id="form_floating_0" class="form-control search-data " name="shipping_no" value=""></div>
                          </th>
                          <th>
                            <div class="form-group form-md-line-input">
                                <select class="form-control search-data " name="agent_name">
                                    <option value="" selected="">--Select One--</option>
                                </select>
                            </div>
                          </th>
                          <th>
                            <div class="form-group form-md-line-input">
                              <select class="form-control search-data " name="status_name">
                                  <option value="" selected="">--Select One--</option>
                              </select>
                            </div>
                          </th>
                          <th colspan="3">
                            <button type="submit" class="btn green submit-search"> 
                              <i class="fa fa-search"></i> Filter
                            </button>
                            <a href="http://localhost/paketin/_admin/shipment/manage-shipment" class="btn green red-mint">
                              <i class="fa fa-refresh"></i> Reset
                            </a>
                          </th>
                        </tr>
                        <tr>
                            <th>No</th>
                            <th><a href="#" class="sorting-data" data-orderby="shipping_no">Shipping No <i class="fa fa-arrows-v"></i></a></th>
                            <th><a href="#" class="sorting-data" data-orderby="agent_name">Agent Name <i class="fa fa-arrows-v"></i></a></th>
                            <th><a href="#" class="sorting-data" data-orderby="status_name">Shipment Status <i class="fa fa-arrows-v"></i></a></th>
                            <th colspan="3"></th>
                        </tr>
                        @foreach($data as $key => $dt)
                          <tr>
                              <td class="vcenter">{{$key + 1}}</td>
                              <td class="vcenter">{{$dt->shipping_no}}</td>
                              <td class="vcenter">{{$dt->agent_name}}</td>
                              <td class="vcenter">{{$dt->status_name}}</td>
                              <td class="vcenter">
                                <!-- <a href="{{url($path)}}/3" class="btn green green-jungle">View</a> -->
                                <a href="{{url($path)}}/{{$dt->id}}/edit" class="btn green ">Edit</a>
  

                                @if(auth()->guard($guard)->user()->station_id == $dt->start_station_id || auth()->guard($guard)->user()->id == 1)
                                  @if($dt->shipment_status_id == 1)
                                    <a href="" class="btn green-sharp" data-toggle="modal" data-target="#exampleModal-{{$dt->id}}" id="btn-assign_driver-{{$dt->id}}">Assign Driver</a>
                                  @elseif($dt->shipment_status_id == 11)
                                    <a href="" class="btn green-sharp" data-toggle="modal" data-target="#exampleModal-{{$dt->id}}" id="btn-assign_driver-{{$dt->id}}">Assign Driver Destination</a>
                                  @elseif($dt->shipment_status_id == 5)
                                    <a href="" class="btn green-sharp" data-toggle="modal" data-target="#exampleModal-{{$dt->id}}" id="btn-confirm_weight_volume-{{$dt->id}}">Confirm Weight Volume</a>
                                  @elseif($dt->shipment_status_id == 8)
                                    <button class="btn green-sharp submit_status" id="arrived_at_station-{{$dt->id}}-{{$dt->shipment_status_id}}">Arrived at Station</button>
                                  @elseif($dt->shipment_status_id == 9)
                                    <button class="btn green-sharp submit_status" id="delivery_to_destination-{{$dt->id}}-{{$dt->shipment_status_id}}">Delivery to Destination</button>
                                  @endif
                                @endif


                                @if(auth()->guard($guard)->user()->station_id == $dt->start_station_id || auth()->guard($guard)->user()->id == 1)
                                  @if($dt->shipment_status_id == 10)
                                    <button class="btn green-sharp submit_status" id="arrived_in_destination_station-{{$dt->id}}-{{$dt->shipment_status_id}}">Arrived in Destination Station</button>
                                  @endif
                                @endif

                                @if($dt->shipment_status_id == 1 || $dt->shipment_status_id == 5)
                                  <!-- <button type="button" class="btn green red-mint submit_status" id="cancel-{{$dt->id}}-{{$dt->shipment_status_id}}">Cancel</button> -->
                                @endif
                              </td>
                          </tr>

                          <!-- modal quick edit -->
                          <div class="modal fade bs-example-modal-lg modal-quick-edit" id="exampleModal-{{$dt->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="exampleModalLabel">
                                    Quick Edit Shipment - {{$dt->shipping_no}}
                                  </h4>
                                  <div class="alert alert-danger" id="warning_danger-{{$dt->id}}" role="alert" style="display: none;"></div>

                                  <div class="alert alert-success" id="warning_success-{{$dt->id}}" role="alert" style="display: none;"></div>
                                </div>
                                  <div class="modal-body">
                                    <input name="_method" type="hidden" value="PATCH">
                                      <div class="row">
                                        <form role="form" class="form-horizontal" method="post" action="{{url($path)}}/ext/postFiles" enctype="multipart/form-data">
                                          <div class="col-md-12">
                                              <h4>Sender Receiver District</h4>
                                          </div>
                                            <div class="col-md-6">
                                                <div class="form-group form-md-line-input" style="padding-top: 0;">
                                                    <label>Start District (include station)</label>
                                                    <select id="start_district-{{$dt->id}}" name="start_district-{{$dt->id}}" class="form-control chosen-select">
                                                      <optgroup label="Start District">
                                                        <option value="">-- Please Select Start District --</option>
                                                        @foreach($city as $o)
                                                          <option value="{{$o->id}}" {{$o->id == $dt->start_district ? 'selected' : ''}}>{{$o->name}}</option>
                                                        @endforeach
                                                      </optgroup>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group form-md-line-input" style="padding-top: 0;">
                                                    <label>Destination District (include station)</label>
                                                    <select id="dest_district-{{$dt->id}}" name="dest_district-{{$dt->id}}" class="form-control chosen-select">
                                                      <optgroup label="Destination District">
                                                         <option value="">-- Please Select Destination District --</option>
                                                        @foreach($city as $o)
                                                          <option value="{{$o->id}}" {{$o->id == $dt->dest_district ? 'selected' : ''}}>{{$o->name}}</option>
                                                        @endforeach
                                                      </optgroup>
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- <div class="col-md-12">
                                                <h4>Destination Location</h4>
                                            </div>

                                            <div class="col-md-12"> 
                                                <input id="pac-input-{{$dt->id}}" class="controls" type="text" placeholder="Search Box">
                                                <div id="map-{{$dt->id}}" class="gmaps_outlet"> </div>
                                                <input type="hidden" name="latitude" id="latbox-{{$dt->id}}" value="{{$dt->latitude}}">
                                                <input type="hidden" name="longitude" id="lngbox-{{$dt->id}}" value="{{$dt->longitude}}">
                                            </div>
                                            <div class="clearfix"></div> -->

                                            @if($dt->shipment_status_id == 1 || $dt->shipment_status_id == 11)
                                              <div class="col-md-12">
                                                <h4>Assign Driver</h4>
                                              </div>

                                              <div class="col-md-6">
                                                  <div class="form-group form-md-line-input" style="padding-top: 0;margin:0;">
                                                      <label>Driver Name (username)</label>
                                                      <select data-placeholder="Select a grade..." class="form-control chosen-select" style="width:350px;" tabindex="2" id="user_id-{{$dt->id}}" name="user_id">
                                                        <option value="">-- Please Select Driver --</option>
                                                        @foreach($driver as $o)
                                                         <option value="{{$o->id}}">{{$o->name.' ('.$o->station_name.')'}}</option>
                                                        @endforeach
                                                      </select>
                                                  </div>
                                              </div>
                                            @endif

                                            @if($dt->shipment_status_id == 5)
                                              <div class="col-md-12">
                                                <h4>Weight and Volume</h4>
                                              </div>

                                              @php
                                                $flagSizeLog = 0;
                                                $volume = null;
                                                $weight = null;
                                                $volume_bef = null;
                                                $weight_bef= null;
                                                if(count($shipment_size_log) > 0){
                                                    foreach($shipment_size_log as $key => $ssl){
                                                        if($ssl->shipment_id == $dt->id){
                                                          if($flagSizeLog == 0){
                                                              $volume = $ssl->volume;
                                                              $weight = $ssl->weight;
                                                              $flagSizeLog = 1;
                                                          }elseif($flagSizeLog == 1){
                                                              $flagSizeLog = 1;
                                                              $volume_bef = $ssl->volume;
                                                              $weight_bef = $ssl->weight;
                                                          }
                                                        }
                                                    }
                                                }
                                                
                                                $long   = ($volume != null ? explode('x',$volume)[0] : '');
                                                $wide   = ($volume != null ? explode('x',$volume)[1] : '');
                                                $height = ($volume != null ? explode('x',$volume)[2] : '');

                                                $long_bef   = ($volume_bef != null ? explode('x',$volume_bef)[0] : '');
                                                $wide_bef   = ($volume_bef != null ? explode('x',$volume_bef)[1] : '');
                                                $height_bef = ($volume_bef != null ? explode('x',$volume_bef)[2] : '');


                                              @endphp

                                             @if(isset($weight_bef))
                                                 <div class="col-md-6">
                                                    <div class="form-group form-md-line-input" qoq="">
                                                      <input type="number" id="form_floating_Qoq" class="form-control" name="weight" value="{{$weight_bef}}" required="" autofocus="" placeholder="Weight" disabled>
                                                      <label for="form_floating_Qoq">Weight Before (Kg)<span class="required" aria-required="true"></span></label>
                                                      <small></small>
                                                    </div>
                                                  </div>
                                              @endif

                                              <div class="col-md-6">
                                                <div class="form-group form-md-line-input" qoq="">
                                                  <input type="number" id="weight-{{$dt->id}}" class="form-control" name="weight" value="{{$weight}}" required="" autofocus="" placeholder="Weight" disabled>
                                                  <label for="form_floating_Qoq">Weight (Kg)<span class="required" aria-required="true"></span></label>
                                                  <small></small>
                                                </div>
                                              </div>


                                                @if(isset($volume_bef))
                                                    <div class="col-md-6">
                                                      <div class="form-group form-md-line-input" qoq="">
                                                        <div class="col-md-4 padding-left-0">
                                                          <input type="number" id="form_floating_Qoq" class="form-control col-md-6" name="long" value="{{$long_bef}}" required="" autofocus="" placeholder="Long" disabled>
                                                        </div>
                                                        <div class="col-md-4 padding-left-0">
                                                          <input type="number" id="form_floating_Qoq" class="form-control " name="wide" value="{{$wide_bef}}" required="" autofocus="" placeholder="Wide" disabled>
                                                        </div>
                                                        <div class="col-md-4 padding-left-0">
                                                          <input type="number" id="form_floating_Qoq" class="form-control " name="height" value="{{$height_bef}}" required="" autofocus="" placeholder="Height" disabled>
                                                        </div>
                                                        <label for="form_floating_Qoq" class="label-volume">Volume Before (pxlxt cm)<span class="required" aria-required="true"></span></label>
                                                        <small></small>
                                                      </div>
                                                    </div>
                                                @endif

                                              <input type="hidden" name="volume" id="volume-{{$dt->id}}" value="{{$volume}}">
                                              <div class="col-md-6">
                                                <div class="form-group form-md-line-input" qoq="">
                                                  <div class="col-md-4 padding-left-0">
                                                    <input type="number" id="form_floating_Qoq" class="form-control col-md-6" name="long" value="{{$long}}" required="" autofocus="" placeholder="Long" disabled>
                                                  </div>
                                                  <div class="col-md-4 padding-left-0">
                                                    <input type="number" id="form_floating_Qoq" class="form-control " name="wide" value="{{$wide}}" required="" autofocus="" placeholder="Wide" disabled>
                                                  </div>
                                                  <div class="col-md-4 padding-left-0">
                                                    <input type="number" id="form_floating_Qoq" class="form-control " name="height" value="{{$height}}" required="" autofocus="" placeholder="Height" disabled>
                                                  </div>
                                                  <label for="form_floating_Qoq" class="label-volume">Volume (pxlxt cm)<span class="required" aria-required="true"></span></label>
                                                  <small></small>
                                                </div>
                                              </div>
                                            @endif
                                        </form>
                                      </div>
                                  </div>
                                  <div class="modal-footer">

                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary submit_status" id="submit_status-{{$dt->id}}-{{$dt->shipment_status_id}}">{{($dt->shipment_status_id == 5 ? 'Accept' : 'Submit')}}</button>
                                    @if($dt->shipment_status_id == 8)
                                        <button type="button" class="btn green red-mint submit_status" id="reject-{{$dt->id}}-{{$dt->shipment_status_id}}">Reject</button>
                                    @elseif($dt->shipment_status_id == 1 || $dt->shipment_status_id == 11)
                                        <button type="button" class="btn green red-mint submit_status" id="cancel-{{$dt->id}}-{{$dt->shipment_status_id}}">Cancel</button>
                                    @endif
                                  </div>
                              </div>
                            </div>
                          </div>
                        @endforeach
                    </tbody>
              </table>
            </div>

        </div>
      <!-- </form> -->

        <!-- modal quick edit -->
        <!-- @foreach($data as $key => $dt) -->
          <!-- <div class="modal fade" id="exampleModal-{{$dt->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="exampleModalLabel">Add Station</h4>
                  <div class="alert alert-danger" id="warning-add-outlet" role="alert" style="display: none;"> </div>

                <div class="alert alert-success" id="success-station" role="alert" style="display: none;"> </div>
                </div>
                <div class="modal-body">
                  <input name="_method" type="hidden" value="PATCH">
                  <div class="row">
                    <form>
                        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'station_name','label' => 'Station Name','value' => (old('station_name') ? old('station_name') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'station_name'])!!}

                    {!!view($view_path.'.builder.text',['type' => 'text','name' => 'phone','label' => 'Phone','value' => (old('phone') ? old('phone') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'phone'])!!}

                    {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'address','label' => 'Address','value' => (old('address') ? old('address') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'address'])!!}
                    </form>
                </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id="submit_station">Submit</button>
                </div>
              </div>
            </div>
          </div> -->
        <!-- @endforeach -->
    </div>
  </div>
@endsection
@push('custom_scripts')
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="{{asset('components/plugins/moment.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('components/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('components/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('components/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('components/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('components/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
  <!-- END PAGE LEVEL PLUGINS -->

  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="{{asset('components/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
  <!-- END PAGE LEVEL PLUGINS -->

  <script>
    jQuery(document).ready(function() {
      $(".submit_status").on("click", function() {
          var id                  = $(this).attr("id");
          id                      = id.split("-");
          var id_shipment         = id[1];
          var shipment_status     = id[2];
          if(id[0] == 'cancel'){
              shipment_status = 'cancel';
          }

          if(id[0] == 'reject'){
              shipment_status = 'reject';
          }

          var user_id             = $("#user_id-"+id_shipment).val();
          var start_district      = $("#start_district-"+id_shipment).val();
          var dest_district       = $("#dest_district-"+id_shipment).val();
          var volume              = $("#volume-"+id_shipment).val();
          var weight              = $("#weight-"+id_shipment).val();

          console.log(shipment_status);
          $.ajax({
              type: "POST",
              url: $.cur_url() + "/ext/change_status",
              data: {id_shipment: id_shipment, shipment_status: shipment_status, user_id: user_id, start_district: start_district, dest_district: dest_district, volume: '', weight: ''},
          }).done(function(msg) {
            console.log(msg);
            if(msg == 'Driver name must be filled !' || msg == 'Start District name must be filled !' || msg == 'Destination District name must be filled !'){
                $('#warning_danger-'+id_shipment).show();
                $('#warning_danger-'+id_shipment).text(msg);
            }else if(msg == '1'){
              $('#warning_success-'+id_shipment).show();
              $('#warning_success-'+id_shipment).text('Success Edit Shipment');
              setTimeout(function(){
                location.reload();
              },1000);
            }

            setTimeout(function(){
              hideWarning();
            },3000);
            // $("#report-content").html(msg);
          });
      });

      function hideWarning(){
          $('#warning_danger').hide();
          $('#warning_success').hide();
      }
    });

    // $('.modal').on('shown.bs.modal', function() {
    //     // alert($(this).attr('id'));
    //     var id                  = $(this).attr("id");
    //     id                      = id.split("-");
    //     var id_shipment         = id[1];
    //     initAutocomplete2(id_shipment);
    // });

    function initAutocomplete2($id) {
        console.log($id);
        var lat = parseFloat(0);
        var lng = parseFloat(0 );


        markers = [];
        map = new google.maps.Map(document.getElementById('map'+$id), {
          zoom: 12,
          styles: [ {"featureType":"all",   "stylers":[     {"saturation":0},     {"hue":"#e7ecf0"}   ] },  {"featureType":"road",    "stylers":[     {"saturation":-70}    ] },  {"featureType":"transit",   "stylers":[     {"visibility":"off"}    ] },  {"featureType":"poi",   "stylers":[     {"visibility":"off"}    ] },  {"featureType":"water",   "stylers":[     {"visibility":"simplified"},      {"saturation":-60}    ] }]
        }); 

        var marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: map,
            draggable:true,
            zoom:12
        });

        markers.push(marker);

        google.maps.event.addListener(marker, 'dragstart', function(event){
            document.getElementById("latbox-"+$id).value = event.latLng.lat();
            document.getElementById("lngbox"+$id).value = event.latLng.lng();
        });

        google.maps.event.addListener(marker, 'dragend', function(event){
            document.getElementById("latbox"+$id).value = event.latLng.lat();
            document.getElementById("lngbox"+$id).value = event.latLng.lng();
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input'+$id);
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          for(var i = 0;i < 1; i++){
            place = places[i];
            var marker = new google.maps.Marker({
              map: map,
              title: place.name,
              position: place.geometry.location,
              draggable:true,
              zoom:1
            });
            markers.push(marker);
            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }

            document.getElementById("latbox"+$id).value = place.geometry.location.lat();
            document.getElementById("lngbox"+$id).value = place.geometry.location.lng();

            google.maps.event.addListener(marker, 'dragstart', function(event){
                document.getElementById("latbox"+$id).value = event.latLng.lat();
                document.getElementById("lngbox"+$id).value = event.latLng.lng();
            });
            google.maps.event.addListener(marker, 'dragend', function(event){
                document.getElementById("latbox"+$id).value = event.latLng.lat();
                document.getElementById("lngbox"+$id).value = event.latLng.lng();
            });
          }
          map.fitBounds(bounds);
        });
    }

     function initAutocomplete() {
        var lat = parseFloat(0);
        var lng = parseFloat(0 );


        markers = [];
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          styles: [ {"featureType":"all",   "stylers":[     {"saturation":0},     {"hue":"#e7ecf0"}   ] },  {"featureType":"road",    "stylers":[     {"saturation":-70}    ] },  {"featureType":"transit",   "stylers":[     {"visibility":"off"}    ] },  {"featureType":"poi",   "stylers":[     {"visibility":"off"}    ] },  {"featureType":"water",   "stylers":[     {"visibility":"simplified"},      {"saturation":-60}    ] }]
        }); 

        var marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: map,
            draggable:true,
            zoom:12
        });

        markers.push(marker);

        google.maps.event.addListener(marker, 'dragstart', function(event){
            document.getElementById("latbox-").value = event.latLng.lat();
            document.getElementById("lngbox").value = event.latLng.lng();
        });

        google.maps.event.addListener(marker, 'dragend', function(event){
            document.getElementById("latbox").value = event.latLng.lat();
            document.getElementById("lngbox").value = event.latLng.lng();
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          for(var i = 0;i < 1; i++){
            place = places[i];
            var marker = new google.maps.Marker({
              map: map,
              title: place.name,
              position: place.geometry.location,
              draggable:true,
              zoom:1
            });
            markers.push(marker);
            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }

            document.getElementById("latbox").value = place.geometry.location.lat();
            document.getElementById("lngbox").value = place.geometry.location.lng();

            google.maps.event.addListener(marker, 'dragstart', function(event){
                document.getElementById("latbox").value = event.latLng.lat();
                document.getElementById("lngbox").value = event.latLng.lng();
            });
            google.maps.event.addListener(marker, 'dragend', function(event){
                document.getElementById("latbox").value = event.latLng.lat();
                document.getElementById("lngbox").value = event.latLng.lng();
            });
          }
          map.fitBounds(bounds);
        });
    }

  </script>
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpMppLOBQyYyKg11P20E9pe4Hs-cwlH9U&libraries=places&callback=initAutocomplete()" async defer></script> -->
@endpush