@extends($view_path.'.layouts.master')
@section('content')

@push('styles')
<style>

</style>

<form role="form" method="post" action="{{url($path)}}/{{$data->id}}" enctype="multipart/form-data">
{{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">        
        <div class="col-md-3">
              <ul class="list-unstyled profile-nav">
                  <li>
                      <img src="{{asset($image_path.'/'.$data->images)}}" onerror="this.src='{{asset($image_path2.'/'.'none.png') }}';" alt="" class="img-responsive">
                  </li>
              </ul>
          </div>
          <!-- <div class="col-md-9">
              <div class="row">
                   <div class="col-md-8 profile-info">
                      <h1 class="font-green sbold uppercase">{{ $data->station_name ? $data->station_name : '' }}</h1>
                        <p>
                          <i class="fa fa-phone"></i> {{ $data->phone ? $data->phone : '' }}
                      </p>
                      <p>
                          <i class="fa fa-map-marker"></i> {{ $data->address ? $data->address : '' }} <br> &nbsp &nbsp{{$data->city_name}}
                      </p>
                  </div>
              </div>
          </div> -->
          <div class="col-md-9">
            <div class="row">
                <div class="col-md-8 profile-info">
                    <h4 class="font-green sbold">{{ $data->station_name ? $data->station_name : '' }}</h4>
                    <ul class="list-inline">
                        <li>
                            <i class="fa fa-phone"></i> {{ $data->phone ? $data->phone : '' }}
                        </li>
                        <li>
                            <i class="fa fa-map-marker"></i> {{ $data->address ? $data->address : '' }}
                        </li>
                         <li>
                            <i class="fa fa-building"></i> {{$data->city_name ? $data->city_name : ''}}
                        </li>
                    </ul>
                </div>
                <!--end col-md-8-->
            </div>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection

@push('custom_scripts')
  <script>
    $(document).ready(function(){
      $('input,select,textarea,checkbox,.remove-single-image').prop('disabled',true);
      tinymce.settings = $.extend(tinymce.settings, { readonly: 1 });
    });
  </script>
@endpush
