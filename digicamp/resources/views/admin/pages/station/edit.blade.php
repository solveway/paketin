@extends($view_path.'.layouts.master')
@section('content')

<form role="form" method="post" action="{{url($path)}}/{{$data->id}}" enctype="multipart/form-data">
	{{ method_field('PUT') }}
	<div class="portlet light bordered">
	    <div class="portlet-title">
	      <div class="caption font-green">
	        <i class="icon-layers font-green title-icon"></i>
	        <span class="caption-subject bold uppercase"> {{$title}}</span>
	      </div>
	      <div class="actions">
	        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
	      </div>
	    </div>
	    <div class="portlet-body form">
	      	@include('admin.includes.errors')
            <div class="row">
  				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'station_name','label' => 'Station Name','value' => (old('station_name') ? old('station_name') : $data->station_name),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}

  				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'phone','label' => 'Phone','value' => (old('phone') ? old('phone') : $data->phone),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}

  				{!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'address','label' => 'Address','value' => (old('address') ? old('address') : $data->address),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}

  				<div class="col-md-6">
		            <div class="">
		                <label>District / City</label>
		                <select id="city" name="city" class="form-control select2">
			                <optgroup label="District / City">
			                  @foreach($city as $o)
			                    <option value="{{$o->id}}" {{$o->id == $data->city_id ? 'selected' : ''}}>{{$o->name}}</option>
			                  @endforeach
			                </optgroup>
		                </select>
		            </div>
		        </div>

  				{!!view($view_path.'.builder.file',['name' => 'images','label' => 'Images','value' => $data->images,'type' => 'file','file_opt' => ['path' => $image_path],'upload_type' => 'single-image','class' => 'col-md-12','note' => 'Note: File Must jpeg,png,jpg,gif','form_class' => 'col-md-12', 'required' => 'n'])!!}
			</div>
			{!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
		</div>
	</div>
</form>
@push('custom_scripts')
	<script>
		$(document).ready(function(){
			
		});
	</script>
@endpush
@endsection