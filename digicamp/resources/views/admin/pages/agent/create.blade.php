@extends($view_path.'.layouts.master')
@section('content')
<style>
 /* .single-image-card{
    max-width: 1024px !important;
    max-height: 714px !important;
  }*/
</style>
<!-- croppie -->
<link rel = "stylesheet" href="{{asset('components/back/css/croppie.css')}}" type="text/css">
<!-- <link rel = "stylesheet" href="{{asset('components/back/css/demo.css')}}" type="text/css"> -->
<!-- croppie -->
@push('styles')
<style>
  .modal-dialog {
    width: 1300px;
    height: 100%;
    margin: 0;
    padding: 0;
  }

  .modal-content {
    height: auto;
    min-height: 100%;
    border-radius: 0;
  }
  
  .canvas-image_card{
    width: 1100px;
    height: 800px;
  }
</style>

<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">
        <div class="col-md-12">
          <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
          <h4 >Business Information</h4>
          </div>
        </div>        
        {!!view($view_path.'.builder.text',['name' => 'business_name','label' => 'Business Name','value' => (old('business_name') ? old('business_name') : ''),'attribute' => 'required', 'form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.textarea',['name' => 'business_address','label' => 'Business Address','value' => (old('business_address') ? old('business_address') : ''),'attribute' => 'required', 'form_class' => 'col-md-6'])!!}

        <div class="col-md-6">
            <div class="">
                <label>District / City <!-- <span class="required" aria-required="true">*</span> --></label>
                <select id="city" name="city" class="form-control select2">
                <optgroup label="District / City">
                  @foreach($city as $o)
                    <option value="{{$o->id}}">{{$o->name}}</option>
                  @endforeach
                </optgroup>
                  </select>
            </div>
        </div>

        {!!view($view_path.'.builder.text',['name' => 'formal_business_name','label' => 'Formal Business Name','value' => (old('formal_business_name') ? old('formal_business_name') : ''),'attribute' => 'required', 'form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.textarea',['name' => 'business_cp_full_name_title','label' => 'Contact Person Full Name & Title','value' => (old('business_cp_full_name_title') ? old('business_cp_full_name_title') : ''),'attribute' => 'required', 'form_class' => 'col-md-6', 'note' => 'example: William Nugraha, Owner'])!!}

        {!!view($view_path.'.builder.text',['name' => 'business_email','label' => 'Business Email','value' => (old('business_email') ? old('business_email') : ''),'attribute' => 'required', 'form_class' => 'col-md-6'])!!}

        <div class="col-md-12">
          <div class="form-group form-md-line-input" hre="">
            <label for="form_floating_hre">Password <span class="required" aria-required="true">*</span></label>
            <div class="input-group">
                  <input type="password" class="form-control password" id="password" name="password" placeholder="Password">
                  <span class="input-group-addon">
                              <a id="show-password" class="text-default"><i class="fa fa-eye"></i></a>
                          </span>

                          <span class="input-group-btn">
                              <a id="generate-password" class="btn btn-primary">Generate</a>
                          </span>
              </div>
          
            <small></small>

          </div>
        </div>

        {!!view($view_path.'.builder.text',['name' => 'business_phone','label' => 'Business Phone','value' => (old('business_phone') ? old('business_phone') : ''),'attribute' => 'required', 'form_class' => 'col-md-6'])!!}

        <div class="form-group form-md-line-input col-md-12">
          <label>Get notified by</label>
        </div>
        <div class="form-group col-md-2">
          <div class="md-checkbox">
              <label>Email</label>
            <input type="checkbox" id="checkbox_form_1" class="md-check email" name="email" value="y" >
            <label for="checkbox_form_1">
                <span></span>
                <span class="check"></span>
                <span class="box"></span>
            </label>
          </div>
        </div>

        <div class="form-group col-md-2">
          <div class="md-checkbox">
              <label>SMS</label>
            <input type="checkbox" id="checkbox_form_2" class="md-check sms" name="sms" value="y" >
            <label for="checkbox_form_2">
                <span></span>
                <span class="check"></span>
                <span class="box"></span>
            </label>
          </div>
        </div>

          <br>
          <div class="col-md-12">
            <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
            <h4>Billing Information</h4>
            </div>
          </div>

          {!!view($view_path.'.builder.textarea',['name' => 'billing_cp_name_title','label' => 'Contact Person Full Name & Title','value' => (old('business_name') ? old('business_name') : ''),'attribute' => 'required', 'form_class' => 'col-md-6', 'note' => 'example: Andi Smith, Owner'])!!}

          {!!view($view_path.'.builder.textarea',['name' => 'billing_address','label' => 'Billing Address','value' => (old('billing_address') ? old('billing_address') : ''),'attribute' => 'required', 'form_class' => 'col-md-6'])!!}

          {!!view($view_path.'.builder.text',['name' => 'billing_email','label' => 'Billing Email','value' => (old('billing_email') ? old('billing_email') : ''),'attribute' => 'required', 'form_class' => 'col-md-6'])!!}

          {!!view($view_path.'.builder.text',['name' => 'billing_phone','label' => 'Billing Phone','value' => (old('billing_phone') ? old('billing_phone') : ''),'attribute' => 'required', 'form_class' => 'col-md-6'])!!}

          <div class="form-group form-md-line-input col-md-12">
            <label>Payment Method</label>
          </div>
          <div class="form-group col-md-2">
            <div class="md-checkbox">
                <label>Weekly Clearing</label>
              <input type="checkbox" id="checkbox_form_3" class="md-check email" name="weekly_clearing" value="y" >
              <label for="checkbox_form_3">
                  <span></span>
                  <span class="check"></span>
                  <span class="box"></span>
              </label>
            </div>
          </div>

          <div class="form-group col-md-2">
            <div class="md-checkbox">
                <label>Monthly Clearing</label>
              <input type="checkbox" id="checkbox_form_4" class="md-check sms" name="monthly_clearing" value="y" >
              <label for="checkbox_form_4">
                  <span></span>
                  <span class="check"></span>
                  <span class="box"></span>
              </label>
            </div>
          </div>

      <div class="col-md-12 actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
      </div>
    </div>
  </div>
</form>

@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    var map;
    $(document).ready(function(){
        var password = $(".password");
        
        $('#generate-password').on('click', function(e){
              var randomstring = Math.random().toString(36).slice(-6);
              password.val(randomstring);
          });

          $('#show-password').on('click', function(e){
              if(password.attr("type") == "password"){
                  password.attr("type", "text");
                  $("#show-password").addClass("text-primary");
                  $("#show-password").removeClass("text-default");
              }
              else{
                  password.attr("type", "password");
                  $("#show-password").addClass("text-default");
                  $("#show-password").removeClass("text-primary");
              }
          });
    });
  </script>
@endpush
@endsection
