@extends($view_path.'.layouts.master')
@section('content')
@section('content')
<style>
 /* .single-image-card{
    max-width: 1024px !important;
    max-height: 714px !important;
  }*/
</style>
<!-- croppie -->
<link rel = "stylesheet" href="{{asset('components/back/css/croppie.css')}}" type="text/css">
<!-- <link rel = "stylesheet" href="{{asset('components/back/css/demo.css')}}" type="text/css"> -->
<!-- croppie -->
@push('styles')
<style>

</style>

<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">        
        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'name','label' => 'Member Type Name','value' => (old('name') ? old('name') : $data->name),'attribute' => 'required autofocus','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'grade','label' => 'Grade','value' => (old('grade') ? old('grade') : $data->grade),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'color','label' => 'Color','value' => (old('color') ? old('color') : $data->color),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'colorpickers'])!!}

        {!!view($view_path.'.builder.text',['type' => 'number','name' => 'limit','label' => 'Limit Province','value' => (old('limit') ? old('limit') : $data->limit),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}

        <div class="col-md-12 actions">
          <!-- {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!} -->
      </div>
  </div>
</form>

@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    var map;
    $(document).ready(function(){
        $('input,select,checkbox,button.remove-single-image,delete-outlet').attr('disabled',true);
    });
  </script>
@endpush
@endsection
