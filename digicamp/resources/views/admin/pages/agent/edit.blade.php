@extends($view_path.'.layouts.master')
@section('content')
@section('content')
<style>
 /* .single-image-card{
    max-width: 1024px !important;
    max-height: 714px !important;
  }*/
</style>
<!-- croppie -->
<link rel = "stylesheet" href="{{asset('components/back/css/croppie.css')}}" type="text/css">
<!-- <link rel = "stylesheet" href="{{asset('components/back/css/demo.css')}}" type="text/css"> -->
<!-- croppie -->
@push('styles')
<style>

</style>

<form role="form" method="post" action="{{url($path)}}/{{$data->id}}" enctype="multipart/form-data">
{{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
          <div class="row">
        <div class="col-md-12">
          <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
          <h4 >Business Information</h4>
          </div>
        </div>        
        {!!view($view_path.'.builder.text',['name' => 'business_name','label' => 'Business Name','value' => (old('business_name') ? old('business_name') : $data->business_name),'attribute' => 'required', 'form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.textarea',['name' => 'business_address','label' => 'Business Address','value' => (old('business_address') ? old('business_address') : $data->business_address),'attribute' => 'required', 'form_class' => 'col-md-6'])!!}

        <div class="col-md-6">
            <div class="">
                <label>District / City <!-- <span class="required" aria-required="true">*</span> --></label>
                <select id="city" name="city" class="form-control select2">
                  <optgroup label="District / City">
                    @foreach($city as $o)
                      <option value="{{$o->id}}" {{$data->city_id == $o->id ? "selected" : ""}}>{{$o->name}}</option>
                    @endforeach
                  </optgroup>
                </select>
            </div>
        </div>

        {!!view($view_path.'.builder.text',['name' => 'formal_business_name','label' => 'Formal Business Name','value' => (old('formal_business_name') ? old('formal_business_name') : $data->formal_business_name),'attribute' => 'required', 'form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.textarea',['name' => 'business_cp_full_name_title','label' => 'Contact Person Full Name & Title','value' => (old('business_cp_full_name_title') ? old('business_cp_full_name_title') : $data->business_cp_full_name_title),'attribute' => 'required', 'form_class' => 'col-md-6', 'note' => 'example: William Nugraha, Owner'])!!}

        {!!view($view_path.'.builder.text',['name' => 'business_email','label' => 'Business Email','value' => (old('business_email') ? old('business_email') : $data->business_email),'attribute' => 'required', 'form_class' => 'col-md-6'])!!}

        <div class="col-md-12">
          <div class="form-group form-md-line-input" hre="">
            <label for="form_floating_hre">Password <span class="required" aria-required="true">*</span></label>
            <div class="input-group">
                  <input type="password" class="form-control password" id="password" name="password" placeholder="Password">
                  <span class="input-group-addon">
                              <a id="show-password" class="text-default"><i class="fa fa-eye"></i></a>
                          </span>

                          <span class="input-group-btn">
                              <a id="generate-password" class="btn btn-primary">Generate</a>
                          </span>
              </div>
          
            <small></small>

          </div>
        </div>

        {!!view($view_path.'.builder.text',['name' => 'business_phone','label' => 'Business Phone','value' => (old('business_phone') ? old('business_phone') : $data->business_phone),'attribute' => 'required', 'form_class' => 'col-md-6'])!!}

        <div class="form-group form-md-line-input col-md-12">
          <label>Get notified by</label>
        </div>
        <div class="form-group col-md-2">
          <div class="md-checkbox">
              <label>Email</label>
            <input type="checkbox" id="checkbox_form_1" class="md-check email" name="email" value="y" {{in_array('email', explode( ';', $data->get_notified_by)) ? 'checked' : ''}}>
            <label for="checkbox_form_1">
                <span></span>
                <span class="check"></span>
                <span class="box"></span>
            </label>
          </div>
        </div>

        <div class="form-group col-md-2">
          <div class="md-checkbox">
              <label>SMS</label>
            <input type="checkbox" id="checkbox_form_2" class="md-check sms" name="sms" value="y" {{in_array('sms', explode( ';', $data->get_notified_by)) ? 'checked' : ''}}>
            <label for="checkbox_form_2">
                <span></span>
                <span class="check"></span>
                <span class="box"></span>
            </label>
          </div>
        </div>

          <br>
          <div class="col-md-12">
            <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
            <h4>Billing Information</h4>
            </div>
          </div>

          {!!view($view_path.'.builder.textarea',['name' => 'billing_cp_name_title','label' => 'Contact Person Full Name & Title','value' => (old('business_name') ? old('business_name') : $data->billing_cp_full_name_title),'attribute' => 'required', 'form_class' => 'col-md-6', 'note' => 'example: Andi Smith, Owner'])!!}

          {!!view($view_path.'.builder.textarea',['name' => 'billing_address','label' => 'Billing Address','value' => (old('billing_address') ? old('billing_address') : $data->billing_address),'attribute' => 'required', 'form_class' => 'col-md-6'])!!}

          {!!view($view_path.'.builder.text',['name' => 'billing_email','label' => 'Billing Email','value' => (old('billing_email') ? old('billing_email') : $data->billing_email),'attribute' => 'required', 'form_class' => 'col-md-6'])!!}

          {!!view($view_path.'.builder.text',['name' => 'billing_phone','label' => 'Billing Phone','value' => (old('billing_phone') ? old('billing_phone') : $data->billing_phone),'attribute' => 'required', 'form_class' => 'col-md-6'])!!}

          <div class="form-group form-md-line-input col-md-12">
            <label>Payment Method</label>
          </div>
          <div class="form-group col-md-2">
            <div class="md-checkbox">
                <label>Weekly Clearing</label>
              <input type="checkbox" id="checkbox_form_3" class="md-check email" name="weekly_clearing" value="y" {{in_array('1', explode( ';', $data->payment_method_id)) ? 'checked' : ''}}>
              <label for="checkbox_form_3">
                  <span></span>
                  <span class="check"></span>
                  <span class="box"></span>
              </label>
            </div>
          </div>

          <div class="form-group col-md-2">
            <div class="md-checkbox">
                <label>Monthly Clearing</label>
              <input type="checkbox" id="checkbox_form_4" class="md-check sms" name="monthly_clearing" value="y" {{in_array('2', explode( ';', $data->payment_method_id)) ? 'checked' : ''}}>
              <label for="checkbox_form_4">
                  <span></span>
                  <span class="check"></span>
                  <span class="box"></span>
              </label>
            </div>
          </div>

      <div class="col-md-12 actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
      </div>
    </div>
  </div>
</form>

@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    var map;
    $(document).ready(function(){
      
    });

    //  function initAutocomplete() {
    //     var lat = parseFloat($('#latbox').val());
    //     var lng = parseFloat($('#lngbox').val());
        
    //     markers = [];
    //     map = new google.maps.Map(document.getElementById('map'), {
    //       zoom: 12,
    //     }); 

    //     var marker = new google.maps.Marker({
    //         position: {lat: lat, lng: lng},
    //         map: map,
    //         draggable:true,
    //         zoom:12
    //     });

    //     markers.push(marker);

    //     google.maps.event.addListener(marker, 'dragstart', function(event){
    //         document.getElementById("latbox").value = event.latLng.lat();
    //         document.getElementById("lngbox").value = event.latLng.lng();
    //     });

    //     google.maps.event.addListener(marker, 'dragend', function(event){
    //         document.getElementById("latbox").value = event.latLng.lat();
    //         document.getElementById("lngbox").value = event.latLng.lng();
    //     });

    //     // Create the search box and link it to the UI element.
    //     var input = document.getElementById('pac-input');
    //     var searchBox = new google.maps.places.SearchBox(input);
    //     map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    //     // Bias the SearchBox results towards current map's viewport.
    //     map.addListener('bounds_changed', function() {
    //       searchBox.setBounds(map.getBounds());
    //     });

    //     // Listen for the event fired when the user selects a prediction and retrieve
    //     // more details for that place.
    //     searchBox.addListener('places_changed', function() {
    //       var places = searchBox.getPlaces();

    //       if (places.length == 0) {
    //         return;
    //       }

    //       // Clear out the old markers.
    //       markers.forEach(function(marker) {
    //         marker.setMap(null);
    //       });
    //       markers = [];

    //       // For each place, get the icon, name and location.
    //       var bounds = new google.maps.LatLngBounds();
    //       for(var i = 0;i < 1; i++){
    //         place = places[i];
    //         var marker = new google.maps.Marker({
    //           map: map,
    //           title: place.name,
    //           position: place.geometry.location,
    //           draggable:true,
    //           zoom:1
    //         });
    //         markers.push(marker);
    //         if (place.geometry.viewport) {
    //           // Only geocodes have viewport.
    //           bounds.union(place.geometry.viewport);
    //         } else {
    //           bounds.extend(place.geometry.location);
    //         }

    //         document.getElementById("latbox").value = place.geometry.location.lat();
    //         document.getElementById("lngbox").value = place.geometry.location.lng();

    //         google.maps.event.addListener(marker, 'dragstart', function(event){
    //             document.getElementById("latbox").value = event.latLng.lat();
    //             document.getElementById("lngbox").value = event.latLng.lng();
    //         });
    //         google.maps.event.addListener(marker, 'dragend', function(event){
    //             document.getElementById("latbox").value = event.latLng.lat();
    //             document.getElementById("lngbox").value = event.latLng.lng();
    //         });
    //       }
    //       map.fitBounds(bounds);
    //     });
    // }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpMppLOBQyYyKg11P20E9pe4Hs-cwlH9U&libraries=places&callback=initAutocomplete" async defer></script>
  <!-- croppie -->
  <script src="{{asset('components/back/js/croppie.js')}}"></script>
  <!-- <script src="{{asset('components/back/js/demo.js')}}"></script> -->
   <!-- <script>
            Demo.init();
        </script> -->
  <!-- croppie -->
@endpush
@endsection
