@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
	<div class="portlet light bordered">
    	<div class="portlet-title">
			<div class="caption font-green">
				<i class="icon-layers font-green title-icon"></i>
				<span class="caption-subject bold uppercase"> {{$title}}</span>
			</div>
			<div class="actions">
				<a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
			</div>
    	</div>
    	<div class="portlet-body form">
      		@include('admin.includes.errors')
  			<div class="row">
  				{!!view($view_path.'.builder.select',['name' => 'user_access_id','label' => 'User Access','value' => (old('user_access_id') ? old('user_access_id') : '1'),'attribute' => 'required','form_class' => 'col-md-12', 'data' => $user_access, 'class' => 'select2', 'onchange' => ''])!!}

  				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'name','label' => 'Name','value' => (old('name') ? old('name') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-12', 'class' => 'name', 'required' => 'y'])!!}

				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'username','label' => 'Username','value' => (old('username') ? old('username') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-12', 'class' => 'username', 'required' => 'y'])!!}				

				<div class="col-md-12">
					<div class="form-group form-md-line-input" hre="">
						<label for="form_floating_hre">Password <span class="required" aria-required="true">*</span></label>
				 		<div class="input-group">
				      		<input type="password" class="form-control password" id="password" name="password" placeholder="Password">
				      		<span class="input-group-addon">
	                            <a id="show-password" class="text-default"><i class="fa fa-eye"></i></a>
	                        </span>

	                        <span class="input-group-btn">
	                            <a id="generate-password" class="btn btn-primary">Generate</a>
	                        </span>
				    	</div>
					
						<small></small>

					</div>
				</div>

				{!!view($view_path.'.builder.text',['type' => 'phone','name' => 'phone','label' => 'Phone','value' => (old('phone') ? old('phone') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}

				{!!view($view_path.'.builder.text',['type' => 'email','name' => 'email','label' => 'Email','value' => (old('email') ? old('email') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}

				<div class="col-md-6">
		          <div class="form-group form-md-line-input">
		            <input type="text" id="birth_date" class="form-control" name="birth_date" value="" readonly="" placeholder="Valid To">
		            <label for="form_floating_Hqd">Birth Date</label>
		            <small></small>
		          </div>
		        </div>

				<!-- {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'description','label' => 'Description','value' => (old('description') ? old('description') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!} -->

				<!-- <div class="col-md-12">
	              <div class="form-group form-md-line-input">
	                <label>Station <span class="required" aria-required="true">*</span></label>
	                <a href="javascript:;" class="btn green" id="add_station" data-toggle="modal" data-target="#exampleModal"> Add
                        <i class="fa fa-plus"></i>
                    </a>
                    <select id="station" name="station[]" class="form-control select2" multiple>
						<optgroup label="Station">
						  @foreach($station as $o)
						  	<option value="{{$o->id}}">{{$o->station_name}}</option>
						  @endforeach
						</optgroup>
                  	</select>
	              </div>
	            </div> -->

	            <div class="col-md-6">
		            <div class="">
		                <label>District / City</label>
		                <select id="city" name="city" class="form-control select2">
		                <optgroup label="District / City">
		                  @foreach($city as $o)
		                    <option value="{{$o->id}}">{{$o->name}}</option>
		                  @endforeach
		                </optgroup>
		                  </select>
		            </div>
		        </div>

	            <!-- <div class="form-group col-md-2">
				  	<div class="md-checkbox">
				  		<label>Login Backend</label>
						<input type="checkbox" id="checkbox_form_1" class="md-check login_web" name="login_web" value="y" >
						<label for="checkbox_form_1">
							<span></span>
					        <span class="check"></span>
					        <span class="box"></span>
						</label>
					</div>
				</div>

				<div class="form-group col-md-2">
					<div class="md-checkbox">
				  		<label>Login App</label>
						<input type="checkbox" id="checkbox_form_2" class="md-check login_app" name="login_app"  value="y">
						<label for="checkbox_form_2">
							<span></span>
					        <span class="check"></span>
					        <span class="box"></span>
						</label>
					</div>
				</div> -->

			  	{!!view($view_path.'.builder.file',['name' => 'picture','label' => 'Picture','value' => '','type' => 'file','file_opt' => ['path' => 'components/admin/images/user/'],'upload_type' => 'single-image','class' => 'col-md-12','note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 138 x 44 px','form_class' => 'col-md-12', 'required' => 'n'])!!}

  				<input type="hidden" id="root-url" value="{{$path}}" />
			</div>	
			{!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
		</div>
	</div>
</form>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="exampleModalLabel">Add Station</h4>
		        <div class="alert alert-danger" id="warning-add-outlet" role="alert" style="display: none;"> </div>

    			<div class="alert alert-success" id="success-station" role="alert" style="display: none;"> </div>
		      </div>
		      <div class="modal-body">
		      	<input name="_method" type="hidden" value="PATCH">
		        <div class="row">
			        <form>
			          	{!!view($view_path.'.builder.text',['type' => 'text','name' => 'station_name','label' => 'Station Name','value' => (old('station_name') ? old('station_name') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'station_name'])!!}

	  					{!!view($view_path.'.builder.text',['type' => 'text','name' => 'phone','label' => 'Phone','value' => (old('phone') ? old('phone') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'phone'])!!}

	  					{!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'address','label' => 'Address','value' => (old('address') ? old('address') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'address'])!!}
			        </form>
			    </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary" id="submit_station">Submit</button>
		      </div>
		    </div>
		  </div>
		</div>
@push('custom_scripts')
	<script>
		$(document).ready(function(){
			var password = $(".password");
			var username = $(".username");
			var name = $(".name");
			
			$('#generate-password').on('click', function(e){
	        	var randomstring = Math.random().toString(36).slice(-6);
		        password.val(randomstring);
		    });

		    $('#show-password').on('click', function(e){
		        if(password.attr("type") == "password"){
		            password.attr("type", "text");
		            $("#show-password").addClass("text-primary");
		            $("#show-password").removeClass("text-default");
		        }
		        else{
		            password.attr("type", "password");
		            $("#show-password").addClass("text-default");
		            $("#show-password").removeClass("text-primary");
		        }
		    });

		    $("#birth_date").datepicker({
		    	changeMonth: true,
		    	changeYear: true,
		    	dateFormat: 'dd-mm-yy',
		    	yearRange: "0:+90",
		    	showButtonPanel: true,

              	onSelect: function(dateText, inst) {

              	}

          	});

		    // $('#myModal').modal('toggle');

          	$('#submit_station').on('click', function(e){
          		var s_name 		= $(".station_name").val();
          		var phone 		= $(".phone").val();
          		var address 	= $(".address").val();
          		var returnval   = 1;
          		$(".alert-danger").hide();
          		$(".alert-success").hide();
          		if(s_name == ''){
          			returnval = 0;
          			$(".alert-danger").show();
          			$("#warning-add-outlet").text('Station Name required');
          		}else if(phone == ''){
          			returnval = 0;
          			$(".alert-danger").show();
          			$("#warning-add-outlet").text('Phone required');
          		}else if(address == ''){
          			returnval = 0;
          			$(".alert-danger").show();
          			$("#warning-add-outlet").text('Address required');
          		}

          		if(returnval == 1){
          			d = {station_name:s_name,phone:phone,address:address};
		        	$.postdata('{{url($path.'/store_station')}}', d).success(function(data){
		        		if(data.status == 'success'){	
		        			$(".alert-success").show();
		        			$("#success-station").text('Success add station');
		        			$(".station_name").val('');
		        			$(".phone").val('');
		        			$(".address").val('');
		        			$('#exampleModal').modal('hide');
		        			location.reload();
		        		}else if(data.status == 'exist'){
		        			$(".alert-danger").show();
		        			$("#warning-add-outlet").text('Station Name exist');

		        		}
		        	});
          		}
		    });

		});
	</script>
@endpush
@endsection