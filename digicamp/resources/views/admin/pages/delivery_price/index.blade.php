@extends($view_path.'.layouts.master')
@push('css')
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="{{asset('components/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('components/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('components/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('components/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('components/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->

  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="{{asset('components/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('components/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->

    <style type="text/css">
      .table-heading{
        border: 1px solid #ddd !important;
      }

       .table.table-light > tbody > tr > td, {
        border: 1px solid #ddd;
      }

      .table.table-light > tbody > tr > td {
        border: 1px solid #ddd !important;
      }

      .table>thead:first-child>tr:first-child>th {
        border: 1px solid #ddd !important;
      }
  </style>
@endpush
@section('content')
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
    </div>
    <div class="portlet-body form">
      <form role="form" class="form-horizontal" method="post" action="{{url($path)}}/ext/postFiles" enctype="multipart/form-data">
        @include('admin.includes.errors')
        <div class="form-body">
          <!-- <div class="row">
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <label class="col-md-4 control-label" for="form_control_1">From City / District</label>
                <div class="col-md-8">
                    <select id="from_city" class="form-control select2-multiple" multiple>
                      <optgroup label="From City / District">
                        <option value="0">-- All -- </option>
                        @foreach($city as $o)
                          <option value="{{$o->id}}" id="from_city-{{$o->id}}">{{$o->name}}</option>
                        @endforeach
                      </optgroup>
                    </select>
                    <div class="form-control-focus"> </div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <label class="col-md-4 control-label" for="form_control_1">To City / District</label>
                <div class="col-md-8">
                    <select id="to_city" class="form-control select2-multiple" multiple>
                      <optgroup label="From City / District">
                        <option value="0">-- All --</option>
                        @foreach($city as $o)
                          <option value="{{$o->id}}" id="to_city-{{$o->id}}">{{$o->name}}</option>
                        @endforeach
                      </optgroup>
                    </select>
                    <div class="form-control-focus"> </div>
                </div>
              </div>
            </div>
          </div> -->
           <div class="row">
              <div class="col-md-12">
                  <label>Import Excel</label><br>
                  <label class="input-file-label-excel">
                      <!-- <input type="file" class="form-control col-md-12 single-excel" name="fileToUpload"> Pilih File -->
                      <input type="file" name="fileToUpload" class="fileToUpload" id="fileToUpload" style="display: block;">
                  </label>
                   
                   <!-- <button type="button" class="btn red-mint remove-single-excel" data-id="single-excel" data-name="excel">Hapus</button> -->
                   <button type="submit" class="btn btn-info" id="submit_excel">Submit</button>
                   <!-- <button type="button" class="btn btn-info" id="submit_excel">Submit</button> -->
                  <input type="hidden" name="remove-single-excel-excel" value="n">
                  <br>
                  <small>Note: File Must .xls .xlsx | Max file size 20M </small>

                   <div class="form-group single-excel-excel col-md-12">
                      
                  </div>
              </div>
              <div class="col-md-12">
                
              </div>
          </div>

          <div class="row">
            <div class="cold-md-12 text-center">
              <input type="button" id="search" value="Generate" class="btn btn-success">
              <a class="btn btn-info" href='{{url($path)}}/ext/exporting'>{{trans('general.export')}}</a>
              <a id="expt" class="hidden">{{trans('general.export')}}</a>
            </div>
          </div>

          <br>
        
          <div class="row">
            <div id="error_row">
                @if(session('error_row'))
                    <div class="clearfix"></div><hr/>
                    <div class="col-md-10 col-md-offset-2"> 
                      <table class="table table-bordered">
                        <th>Baris</th>
                        <th>Keterangan</th>
                        <tbody>
                          @foreach(session('error_row') as $eq => $ev)
                            <tr>
                              <td>{{$eq + 1}}</td>
                              <td>
                                @if(isset($ev['limit_user']))
                                  Jumlah user yang dibuat sudah melebihi limit
                                  <br/>
                                @endif
                                @if(isset($ev['user_group']))
                                  User group tidak ditemukan di database
                                  <br/>
                                @endif
                                @if(isset($ev['location']))
                                  Location tidak ditemukan di database
                                  <br/>
                                @endif
                                @if(isset($ev['timetable_creation_template']))
                                  Timetable schedule template tidak ditemukan di database
                                  <br/>
                                @endif
                                @if(isset($ev['required']))
                                  Kolom city_from, from_city_id, city_to, to_city_id, kolom price tidak boleh kosong
                                  <br/>
                                @endif
                                @if(isset($ev['username']))
                                  Username {{$ev['username_value']}} sudah ada dan tidak boleh sama
                                  <br/>
                                @endif
                                @if(isset($ev['citynotfound']))
                                  City Id {{$ev['citynotfound_value']}} tidak ditemukan
                                  <br/>
                                @endif
                                @if(isset($ev['email']))
                                  Email {{$ev['email_value']}} sudah ada dan tidak boleh sama
                                @endif
                              </td>
                            </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                @endif
            </div>
            <div id="report-content" class="col-md-12">
              
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection
@push('custom_scripts')
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="{{asset('components/plugins/moment.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('components/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('components/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('components/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('components/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('components/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
  <!-- END PAGE LEVEL PLUGINS -->

  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="{{asset('components/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
  <!-- END PAGE LEVEL PLUGINS -->

  <script>
    jQuery(document).ready(function() {
      $('#reportrange').daterangepicker({
        opens: (App.isRTL() ? 'left' : 'right'),
        startDate: moment().subtract('days', 29),
        endDate: moment(),
        dateLimit: {
            days: 60
        },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
            'Last 7 Days': [moment().subtract('days', 6), moment()],
            'Last 30 Days': [moment().subtract('days', 29), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
        },
        buttonClasses: ['btn'],
        applyClass: 'green',
        cancelClass: 'default',
        format: 'DD-MM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Apply',
            format: 'DD-MM-YYYY',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
      },
        function (start, end) {
            $('#reportrange span').html(start.format('DD-MM-YYYY') + ' s/d ' + end.format('DD-MM-YYYY'));
        }
      );
      //Set the initial state of the picker label
      $('#reportrange span').html(moment().subtract('days', 29).format('DD-MM-YYYY') + ' s/d ' + moment().format('DD-MM-YYYY'));

      //select 2
      $(".select2-multiple").select2({
          placeholder: "Klik di sini",
          width: null
      });

      $(".select2-multiple").on("select2:open", function() {
          if ($(this).parents("[class*='has-']").length) {
              var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

              for (var i = 0; i < classNames.length; ++i) {
                  if (classNames[i].match("has-")) {
                      $("body > .select2-container").addClass(classNames[i]);
                  }
              }
          }
      });

      // $('#outlet').change(function(){
      //     var val     = $('#outlet').val();

      //     $("#operator").children('option').hide();
      //     $("#operator").removeAttr("disabled");

      //     var operator_arr = JSON.parse($('#operator_arr').val());
      //     if(operator_arr.length > 0){
      //       for(var i=0; i<operator_arr.length; i++){
      //           var outlet_id   =  operator_arr[i]['outlet_id'];
      //           outlet_id = $.formatSeparatorToArray(outlet_id, ';');
      //           if(outlet_id.length > 0){
      //               for(var j=0; j<outlet_id.length; j++){
      //                   if(outlet_id[j] == val){
      //                       $("#operator").children('option[value^="'+operator_arr[i]['id']+'"]').show();
      //                   }
      //               }
      //           }
      //       }
      //     }
      // });

      $("#search").on("click", function() {
          // var fromCity      = $("#from_city").val();
          // var toCity        = $("#to_city").val();
          console.log("#search");
          $.ajax({
              type: "POST",
              url: $.cur_url() + "/ext/filter",
              data: { from_city: '', to_city: ''},
          }).done(function(msg) {
            console.log(msg);
            $("#report-content").html(msg);
          });
      });

      $("#submit_excel").on("click", function() {
          var action        = "import";
          var form_data     = new FormData($('#submit_excel')[0]);
          console.log("#search");
          $.ajax({
              type: "POST",
              url: $.cur_url() + "/ext/filter",
              data: { from_city: fromCity, to_city: toCity},
          }).done(function(msg) {
            console.log(msg);
            $("#report-content").html(msg);
          });
      });

    });

    
  </script>
@endpush