@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$data->id}}" enctype="multipart/form-data">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
        @include('admin.includes.errors')
        <div class="row">        
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'service_name','label' => 'Service Name','value' => (old('service_name') ? old('service_name') : $data->service_name),'attribute' => 'required autofocus','form_class' => 'col-md-6'])!!}

            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'price','label' => 'Service Price (Rp.)','value' => (old('service_price') ? old('price') : $data->price),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'service_price'])!!}

            <div class="col-md-6">
              <div class="form-group form-md-line-input"></div>
                <label for="tag">Service Category <span class="required" aria-required="true"></span></label>
                <select class="select2" name="service_category" class="service_category" id="service_category">
                    @foreach($service_category as $sc)
                      <option value="{{$sc->category_service_name}}" {{old('service_category') ? (in_array($sc,[old('unit')]) ? 'selected' : '') : ($data->service_category ? ($data->service_category == $sc->category_service_name ? 'selected' : '') : '' )}}>{{$sc->category_service_name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr/>
        <div class="row">
              <div class="col-md-12" style="padding-left: 0;">  

                <div class="form-group col-md-6">
                  <label for="tag">Product <span class="required" aria-required="true"></span></label>
                  <select class="select2" name="product" class="product" id="product">
                      <option value="0">Select Product</option>
                      @foreach($product as $p)
                        <option value="{{$p->id}}">{{$p->product_name}}</option>
                      @endforeach
                  </select>
                </div>

                <div class="col-md-6">

                  <div class="form-group form-md-line-input" rrc="">

                    <input type="number" class="form-control qty_default" name="qty_default" id="qty_default" value="" placeholder="Qty Default">

                    <label for="form_floating_RRC">Qty Default <span class="required" aria-required="true"></span></label>

                    <small></small>

                  </div>
                </div>
              </div>

              <div class="col-md-12">

                {!!view($view_path.'.builder.button',['type' => 'button','label' => 'Add','class' => 'add-product'])!!}

              </div>

              <hr/>

              <div class="table-responsive redeem-auto col-md-12">

                <table class="table table-bordered">

                  <thead>
                    <th>Product Name</th>
                    <th>Qty Default</th>
                    <th>Action</th>

                  </thead>

                  <tbody class="product-data">
                      @if(count($data2) > 0)
                        @foreach($data2 as $sp)
                          <tr>
                            <td>{{$sp->product_name}}<input type="hidden" name="product_id[]" value="{{$sp->product_id}}"></td>
                            <td>{{$sp->qty_default}}<input type="hidden" name="qty_default[]" value="{{$sp->qty_default}}"></td>
                            <td><button type="button" class="btn btn-danger delete-product"><i class="fa fa-trash"></i></button></td>
                          </tr>
                        @endforeach
                      @endif
                  </tbody>

                </table>

              </div>

              <div class="col-md-12 actions">
                {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
              </div>
        </div>
    </div>
  </div>
</form>
@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
      //onclick event add remove outlet
      $(document).on('click','.add-product',function(){
        var product_name      = $('#product option:selected').text();
        var product_id        = $('#product').val();
        var qty_default       = $('#qty_default').val();
        console.log(product_name);
        console.log(qty_default);
        if(product_id == 0){
            alert('Please select product !');
        }else if(qty_default < 1 ){
            alert('Qty default must greater than 0 !');
        }else{
            var temp              = '<tr><td>'+product_name+'<input type="hidden" name="product_id[]" value="'+product_id+'"></td><td>'+qty_default+'<input type="hidden" name="qty_default[]" value="'+qty_default+'"></td><td><button type="button" class="btn btn-danger delete-product"><i class="fa fa-trash"></i></button></td></tr>';
            $('.product-data').append(temp);
        }
      })

      $(document).on('click','.delete-product',function(){
        $(this).closest('tr').remove();
      });

      $( ".service_price" ).blur(function() {  
          // alert('test');
          //number-format the user input
          var val   = $(this).val();
          var val2  = $.formatRupiah(val);
          $(this).val(val2);             
      });
    });
  </script>
@endpush
@endsection
