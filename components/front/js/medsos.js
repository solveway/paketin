$(document).ready(function(){
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.9";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));


	$('.fb-share').click(function(e) {
        e.preventDefault();
        window.open($(this).attr('href'), 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
        return false;
    });

    $('.twitter-share').click(function(e) {
	    e.preventDefault();
	    var href = $(this).attr('href');
	    window.open(href, "Twitter", "height=450,width=550,resizable=1");
	    return false;
	});

	$('.google-share').click(function(e) {
	    e.preventDefault();
	    var href = $(this).attr('href');
	    window.open(href, "", "height=600,width=600,resizable=1,scrollbars=yes,toolbar=no,menubar=no");
	    return false;
	});
});